INC_DIR=inc
SRC_DIR=src
OBJ_DIR=obj
BIN_DIR=bin

CC=clang

ifeq ($(CC),)
	CC=clang
endif
AR=ar

ifeq ($(CC),clang)
	WALL=everything
	WEXTRA=-Wno-switch-enum -Wno-reserved-id-macro -Wno-vla -Wno-covered-switch-default
else
	WALL=all
	WEXTRA=-Wno-switch
endif

WARN=-W$(WALL) -Wno-unknown-pragmas $(WEXTRA)

OUTNAME=dzesamgihe
OUTPATH=$(BIN_DIR)/$(OUTNAME)

FLOPPY=0
ifeq ("$(wildcard /dev/gpiomem)","")
	CDEFS ?= -DDSG_DISABLE_FLOPPY
	FLOPPY=1
endif

CFLAGS=$(CDEFS) -I$(INC_DIR) -std=gnu11 $(WARN) -pthread
LIBS=-lm -lpthread -lportaudio -lrtmidi

default: debug

all: makeobjdirs
all: $(OUTPATH)

$(OUTPATH): $(OBJ_DIR)/fastmath.o $(OBJ_DIR)/gpio.o $(OBJ_DIR)/bufqueue.o \
    $(OBJ_DIR)/midi.o $(OBJ_DIR)/osc.o $(OBJ_DIR)/chan.o $(OBJ_DIR)/player.o \
    $(OBJ_DIR)/render.o $(OBJ_DIR)/biquad.o $(OBJ_DIR)/floppy.o $(OBJ_DIR)/dsg.o \
    $(OBJ_DIR)/fe_track.o $(OBJ_DIR)/fe_midi.o $(OBJ_DIR)/net_lc.o $(OBJ_DIR)/main.o
	$(CC) -o $@ \
        $(OBJ_DIR)/fastmath.o $(OBJ_DIR)/gpio.o $(OBJ_DIR)/bufqueue.o $(OBJ_DIR)/midi.o \
        $(OBJ_DIR)/osc.o $(OBJ_DIR)/chan.o $(OBJ_DIR)/player.o $(OBJ_DIR)/render.o \
        $(OBJ_DIR)/biquad.o $(OBJ_DIR)/floppy.o $(OBJ_DIR)/dsg.o \
        $(OBJ_DIR)/fe_track.o $(OBJ_DIR)/fe_midi.o $(OBJ_DIR)/net_lc.o $(OBJ_DIR)/main.o \
        $(CFLAGS) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

debug: CFLAGS += -DDEBUG -g
debug: all

release: CFLAGS += -DRELEASE -O3
release: cleanbins all

makeobjdirs:
	@if ! [ -d "$(BIN_DIR)" ]; then	mkdir -p "$(BIN_DIR)"; fi
	@if ! [ -d "$(OBJ_DIR)" ]; then	mkdir -p "$(OBJ_DIR)"; fi

cleanbins:
	@if [ -f "$(OUTPATH)" ]; then	rm "$(OUTPATH)"; fi

clean: cleanbins
	-find "$(OBJ_DIR)" -type f -name "*.o" | xargs rm -v

.PHONY: clean all debug release

