
#define _DEFAULT_SOURCE

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <rtmidi/rtmidi_c.h>

#include "config.h"
#include "player.h"

#include "frontend.h"
#include "dsg.h"

#pragma clang diagnostic ignored "-Wunused-parameter"

#include "net_lc.h"

int main() {
    return dsg_net_lc_main(((struct dsg_chan_hard_floppy_opts){
        .pin_dir_phys  = 0x0B,
        .pin_step_phys = 0x0C
    }), /*"::1",*/ 1337);
}

/*struct mapped_file
{
    void* map;
    size_t size;
    int filed;
};

static struct mapped_file map_file(char* filep, bool share)
{
    int filed = open(filep, O_RDONLY);

    if (filed < 1)
        return (struct mapped_file){.map=NULL,.filed=filed,.size=0};

    struct stat stat;
    if (fstat(filed, &stat))
    {
        close(filed);
        return (struct mapped_file){.map=NULL,.filed=0,.size=0};
    }
    size_t size = (size_t)stat.st_size;

    void* map = mmap(NULL, size, PROT_READ, share ? MAP_SHARED : MAP_PRIVATE, filed, 0);
    if (!map || map == MAP_FAILED)
    {
        close(filed);
        return (struct mapped_file){.map=NULL,.filed=0,.size=0};
    }

    return (struct mapped_file){.map=map,.filed=filed,.size=size};
}
static void unmap_file(struct mapped_file map)
{
    if (map.filed <= 0)
        return;

    if (map.map && map.size)
        munmap(map.map, map.size);

    close(map.filed);
}

int main(int argc, char* argv[])
{
    //return test_midi(1);

    char* trackf = "track.bin" ;
    char* conff  = "config.bin";
    if (argc > 1) trackf = argv[1];
    if (argc > 2) conff  = argv[2];

    struct mapped_file trackm = map_file(trackf, true );
    struct mapped_file confm  = map_file(conff , false);

    if (!trackm.map)
    {
        if (confm.map)
            unmap_file(confm);

        printf("E: cannot mmap %s\n", trackf);
        return 1;
    }
    if (!confm.map)
    {
        if (trackm.map)
            unmap_file(trackm);

        printf("E: cannot mmap %s\n", conff);
        return 1;
    }

    int retv = 0;

    struct dsg_track_entry* track = (struct dsg_track_entry*)trackm.map;
    struct dsg_config     * conf  = (struct dsg_config     *)confm .map;

    size_t rows = dsg_get_rows(trackm.size, conf);

    // TODO: read this from a config file
    #define MIDI_PORT_AMT (1)

    struct dsg_midi_port ports[MIDI_PORT_AMT];
    ports[0] = (struct dsg_midi_port)
    {
        .portnum = 1,

        .ignore_sysex = false,
        .ignore_time  = false,
        .ignore_sense = false
    };
    / *ports[1] = (struct dsg_midi_port)
    {
        .portnum = 1,

        .ignore_sysex = true,
        .ignore_time  = true,
        .ignore_sense = true
    };* /
    / *ports[2] = (struct dsg_midi_port)
    {
        .portnum = 2,

        .ignore_sysex = false,
        .ignore_time  = false,
        .ignore_sense = false
    };

    void* dsg_data;
    if (!(dsg_data = dsg_init(//dsg_make_track_frontend(track, rows))))
                              dsg_make_midi_frontend(MIDI_PORT_AMT, ports))))
    {
        printf("E: Cannot init.\n");

        retv = 1;
        goto FREE_ALL;
    }

    if (dsg_run(conf, dsg_data))
    {
        printf("E: dsg_run exited with a failure.\n");

        retv = 1;
        goto FREE_ALL;
    }

FREE_ALL:
    dsg_kill(dsg_data);

    unmap_file(confm );
    unmap_file(trackm);

    return retv;
}*/

