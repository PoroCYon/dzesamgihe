
#ifdef DSG_DISABLE_FLOPPY
#pragma clang diagnostic ignored "-Wempty-translation-unit"
#warning "Floppy drive support disabled."
#else

#define _DEFAULT_SOURCE

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

#include "fastmath.h"
#include "gpio.h"

#include "floppy.h"


// NOTE: UGLY HACK
#ifdef SAMPLE_RATE
#undef SAMPLE_RATE
#define SAMPLE_RATE (5512)
#endif

struct floppy_thread
{
    volatile struct dsg_floppy_ctl* ctl;
    dsg_rpi_gpio gpio;
    pthread_t thread;
    pthread_mutex_t mutex;
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"

volatile struct dsg_floppy_ctl* dsg_floppy_make(
    struct dsg_chan_hard_floppy_opts opts)
{
    volatile struct dsg_floppy_ctl* retv =
        (volatile struct dsg_floppy_ctl*)
        calloc(1, sizeof(struct dsg_floppy_ctl));

    retv->pin_dir_bcm  = dsg_rpi_convert_pin(
        opts.pin_dir_phys , dsg_gpio_pin_physical, dsg_gpio_pin_broadcom);
    retv->pin_step_bcm = dsg_rpi_convert_pin(
        opts.pin_step_phys, dsg_gpio_pin_physical, dsg_gpio_pin_broadcom);

    retv->quit_thread = false;

    return retv;
}
void dsg_floppy_free(volatile struct dsg_floppy_ctl* ctl)
{
    if (!ctl)
        return;

    dsg_buf_free(ctl->queue, true);

    free((void*)ctl);
}

void dsg_floppy_enqueue(dsg_floppy_bg_thread thread,
    size_t buf_size, float* buf, bool no_copy)
{
    if (!thread)
        return;

    struct floppy_thread* t = (struct floppy_thread*)thread;

    if (!t->ctl) return;

    pthread_mutex_lock(&t->mutex);

    dsg_buf_enqueue(&t->ctl->queue, buf_size, buf, no_copy);

    pthread_mutex_unlock(&t->mutex);
}

struct dsg_buf dsg_floppy_dequeue(dsg_floppy_bg_thread thread)
{
    static struct dsg_buf fail = (struct dsg_buf){
        .buf     = NULL ,
        .size    = 0    ,
        .is_copy = false
    };

    if (!thread)
        return fail;

    struct floppy_thread* t = (struct floppy_thread*)thread;

    if (!t->ctl->queue)
        return fail;

    pthread_mutex_lock(&t->mutex);

    struct dsg_buf retv = dsg_buf_dequeue(&t->ctl->queue);

    pthread_mutex_unlock(&t->mutex);

    return retv;
}

#ifndef NO_BLOCKING_SLEEP

#ifndef ENABLE_BLOCKING_SLEEP_OPT
#pragma GCC push_options
#pragma GCC optimize("O0")
#endif

inline static void blocking_sleep(size_t itern)
{
    size_t iterm1 = itern - 1;
    for (size_t i = itern; i & iterm1; --i)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wlanguage-extension-token"
        asm volatile("nop");
#pragma clang diagnostic pop
}

#ifndef ENABLE_BLOCKING_SLEEP_OPT
#pragma GCC pop_options
#endif

#else
#define blocking_sleep(itern) {};
#endif

//#define MAX_STEPS (0x20)

static void floppy_bg_reset(struct floppy_thread* thread)
{
    volatile struct dsg_floppy_ctl* ctl = thread->ctl;

    dsg_rpi_gpio gpio = thread->gpio;

    dsg_gpio_set_pin_dir(gpio, ctl->pin_dir_bcm , dsg_gpio_pin_output);
    dsg_gpio_set_pin_dir(gpio, ctl->pin_step_bcm, dsg_gpio_pin_output);

    dsg_gpio_set_pin(gpio, ctl->pin_dir_bcm, false);
    for (size_t i = 0; i < 0x20; ++i)
    {
        dsg_gpio_set_pin(gpio, ctl->pin_step_bcm, true );

        blocking_sleep(0x10000);

        dsg_gpio_set_pin(gpio, ctl->pin_step_bcm, false);

        usleep(0xC0);
    }

    ctl->cur_dir  = true;
    ctl->cur_freq = 0.0f;
    dsg_gpio_set_pin(gpio, ctl->pin_dir_bcm, true );

    for (size_t i = 0; i < 0x08; ++i)
    {
        dsg_gpio_set_pin(gpio, ctl->pin_step_bcm, true );

        blocking_sleep(0x10000);

        dsg_gpio_set_pin(gpio, ctl->pin_step_bcm, false);

        usleep(0xC0);
    }
}

static void floppy_bg_cycle(struct floppy_thread* thread, int64_t dur_ns)
{
    volatile struct dsg_floppy_ctl* ctl = thread->ctl;

    size_t pdir  = ctl->pin_dir_bcm ,
           pstep = ctl->pin_step_bcm;

    //if (ctl->cur_step == MAX_STEPS)
    //{
    //    ctl->cur_dir = !ctl->cur_dir;
        dsg_gpio_set_pin(thread->gpio, pdir, ctl->cur_dir);
        ctl->cur_dir = !ctl->cur_dir;

    //    ctl->cur_step = 0;
    //}

    //++ctl->cur_step;

    struct timespec ts, te;

    clock_gettime(CLOCK_REALTIME, &ts);
    dsg_gpio_set_pin(thread->gpio, pstep, true );

    // don't use usleep, as the thread mustn't be cancelled
    // while the step pin's high (preferably)
    blocking_sleep(0x10000);

    dsg_gpio_set_pin(thread->gpio, pstep, false);
    clock_gettime(CLOCK_REALTIME, &te);

    int64_t dt = (te.tv_sec - ts.tv_sec) * NSEC_IN_SEC + te.tv_nsec - ts.tv_nsec;
    dur_ns -= dt;

    if (dur_ns < 0) {
        printf("took too long\n");
        return;
    }

    int64_t modNS = dur_ns % NSEC_IN_SEC;
    ts.tv_nsec = (ssize_t)modNS;
    ts.tv_sec  = (time_t)((dur_ns - modNS) / NSEC_IN_SEC);

    nanosleep(&ts, NULL);
}

static void floppy_bg_thread(struct floppy_thread* thread)
{
    volatile struct dsg_floppy_ctl* ctl = thread->ctl;

    floppy_bg_reset(thread);

    size_t i = 0;

    while (!ctl->quit_thread)
    {
        // wait for something
        while (ctl->cur_freq == 0.0f && (!ctl->queue || !ctl->queue->buf.buf))
            usleep(0x100);

        if (!ctl->queue)
        {
            float f = ctl->cur_freq;

            if (f == 0.0f || f >= (SAMPLE_RATE >> 1))
            {
                size_t del_i = 0x60;
                usleep(((uint32_t)(del_i * (float)USEC_IN_SEC / SAMPLE_RATE) * 7) >> 3);
            }
            else
            {
                float f_rcp = 1.0f / f;
                //del_i = (size_t)(SAMPLE_RATE * f_rcp);

                floppy_bg_cycle(thread, (int64_t)(NSEC_IN_SEC * f_rcp));
            }

            continue;
        }

        struct dsg_buf buf = dsg_floppy_dequeue(thread);

        if (i >= buf.size)
            i %= buf.size;

        for (size_t del_i = 1; i < buf.size && !ctl->quit_thread; i += del_i)
        {
            float f = 0.0f;

            for (size_t j = 0; j < 0x10 && j + i < buf.size; ++j)
                if (buf.buf[j] != 0.0f && buf.buf[j] < ((size_t)SAMPLE_RATE >> 1))
                {
                    f = buf.buf[j];
                    break;
                }

            if (f == 0.0f || f >= (SAMPLE_RATE >> 1) /* above nyquist freq */)
            {
                del_i = min(0x10, buf.size - i);

                usleep(((uint32_t)(del_i * (float)USEC_IN_SEC / SAMPLE_RATE) * 7) >> 3);
            }
            else
            {
                float f_rcp = 1.0f / f;
                del_i = (size_t)(SAMPLE_RATE * f_rcp);

                floppy_bg_cycle(thread, (int64_t)(NSEC_IN_SEC * f_rcp));
            }
        }

        if (buf.is_copy)
            free(buf.buf);
    }
}
static void* _bg_thread_stub(void* foo)
{
    floppy_bg_thread((struct floppy_thread*)foo);

    return NULL;
}

void dsg_floppy_set_freq(dsg_floppy_bg_thread thread, float freq)
{
    if (!thread) return;

    struct floppy_thread* t = (struct floppy_thread*)thread;

  //pthread_mutex_lock(&t->mutex);

    t->ctl->cur_freq = freq;

  //pthread_mutex_lock(&t->mutex);
}

dsg_floppy_bg_thread dsg_floppy_spawn_bg_thread(volatile struct dsg_floppy_ctl* ctl, dsg_rpi_gpio gpio)
{
    if (!ctl) return NULL;

    ctl->quit_thread = false;
    ctl->cur_dir     = false;

    ctl->cur_step = 0;
    ctl->cur_freq = 0.0f;

    struct floppy_thread* thread = (struct floppy_thread*)calloc(1, sizeof(struct floppy_thread));

    thread->ctl  = ctl ;
    thread->gpio = gpio;

    pthread_mutex_init(&thread->mutex, NULL);

    if (pthread_create(&thread->thread, NULL, _bg_thread_stub, (void*)thread))
    {
        pthread_mutex_destroy(&thread->mutex);

        free(thread);

        return NULL;
    }

    return thread;
}
void dsg_floppy_kill_bg_thread(dsg_floppy_bg_thread thread)
{
    if (!thread) return;

    struct floppy_thread* t = (struct floppy_thread*)thread;

    t->ctl->quit_thread = true;

    pthread_mutex_destroy(&t->mutex);

    pthread_cancel(t->thread);
}

#pragma clang diagnostic pop

#endif

