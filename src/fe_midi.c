
// TODO:
// * fix arps
// * fix buffer underflows
//
// * vol delta speed?

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#include "player.h"
#include "render.h"
#include "frontend.h"
#include "floppy.h"

#include <rtmidi/rtmidi_c.h>

enum arp_type
{
    arp_none, arp_s, arp_m, arp_r
};
struct midi_init
{
    size_t port_amt;
    struct dsg_midi_port* ports;
};
struct midi_data;
struct rtm_data
{
    struct midi_data* data;
    uint32_t portnum;
};
struct midi_data
{
    struct midi_init* init;
    RtMidiInPtr* midi_ins;
    struct rtm_data* rtmd;
    struct dsg_player* player;
    enum arp_type* arps;
#ifndef DSG_DISABLE_FLOPPY
    dsg_floppy_bg_thread* bgt;
#endif
};

#pragma clang diagnostic ignored "-Wunused-parameter"

static void rtm_callback(double dt, const uint8_t* data, void* userdata)
{
    const struct dsg_midi_raw* rmid = (const struct dsg_midi_raw*)data;
    struct rtm_data* rtm = (struct rtm_data*)userdata;
    struct midi_data* mdt = rtm->data;

    struct dsg_player* player = rtm->data->player;

    if (!player) // || (rmid->status & 0x0F) >= player->config->chan_amt)
        return;

    uint16_t effv  = 0;
    size_t   chann = 0;

    // TODO: make it work with more than just the hardware I'm using
    switch (rmid->status & 0xF0)
    {
        case dsg_midi_note_on:
            if (!rmid->data[1])
                goto NOTE_OFF_CASE;
            chann = dsg_get_chan_from_note(rmid->data[0], mdt->player->config);
            //printf("note on %X\n", rmid->data[0]);
            if (!~chann)
                chann = rmid->status & 0x0F;
#ifndef DSG_DISABLE_FLOPPY
            else if (mdt->player->config->chan_cfg[chann].type == dsg_chan_hard_floppy)
            {
                dsg_floppy_set_freq(mdt->bgt[chann], dsg_note_to_freq(rmid->data[0] + 0x19));
                break;
            }
#endif
            effv = (uint16_t)((float)rmid->data[1] * (1.0f / 127.0f) * 4095.0f);
            dsg_player_upd_chan(player, chann, ((struct dsg_track_entry)
            {
                .meta_event = dsg_meta_nothing | dsg_event_note_on,
                .note_ctl   = rmid->data[0],
                .effect_1   = (uint8_t)(dsg_effect_volume_temp | ((effv & 0xF00) >> 8)),
                .effect_2   = (uint8_t)(effv & 0x0FF)
            }));
            break;
        case dsg_midi_note_off:
NOTE_OFF_CASE:
            //printf("note off %X\n", rmid->data[0]);
            chann = dsg_get_chan_from_note(rmid->data[0], mdt->player->config);
            if (!~chann)
                chann = rmid->status & 0x0F;
#ifndef DSG_DISABLE_FLOPPY
            else if (mdt->player->config->chan_cfg[chann].type == dsg_chan_hard_floppy)
            {
                dsg_floppy_set_freq(mdt->bgt[chann], 0.0f);
                break;
            }
#endif
            dsg_player_upd_chan(player, chann, ((struct dsg_track_entry)
            {
                .meta_event = dsg_meta_nothing | dsg_event_note_off,
                .note_ctl   = rmid->data[0],
                .effect_1   = 0x00,
                .effect_2   = 0x00
            }));
            break;
        case dsg_midi_cchange:
            {
                uint8_t cc = rmid->data[0],
                        cv = rmid->data[1];

                printf("CC %X - %X\n", cc, rmid->data[1]);

                if (cc < 0x14 && cc >= 0x10 && ((ssize_t)cc - 0x10) < (ssize_t)player->config->chan_amt)
                {
                    uint16_t v = 0;

                    if (cv < 0x40)
                        v = (uint16_t)(0x800 | (uint8_t)(0x40 - cv));
                    else
                        v = (uint16_t)(cv - 0x40);

                    dsg_player_upd_chan(player, cc & 0003, ((struct dsg_track_entry)
                    {
                        .meta_event = dsg_meta_nothing | dsg_event_nochange,
                        .note_ctl   = 0,
                        .effect_1   = (uint8_t)(dsg_effect_extra_pitch | ((v & 0xF00) >> 8)),
                        .effect_2   = (uint8_t)(v & 0x0FF)
                    }));
                }
                else if (cc < 0x04 && cc < player->config->chan_amt)
                {
                    uint16_t v = (uint16_t)(cv * (4095.0f / 127.0f));

                    dsg_player_upd_chan(player, cc & 0003, ((struct dsg_track_entry)
                    {
                        .meta_event = dsg_meta_nothing | dsg_event_nochange,
                        .note_ctl   = 0,
                        .effect_1   = (uint8_t)(dsg_effect_duty_cycle | ((v & 0xF00) >> 8)),
                        .effect_2   = (uint8_t)(v & 0x0FF)
                    }));
                }
                else if (cc < 0x08 && cc >= 0x04 && ((ssize_t)cc - 0x04) < (ssize_t)player->config->chan_amt)
                {
                    uint16_t v = (uint16_t)(cv * (4095.0f / 127.0f));

                    dsg_player_upd_chan(player, cc & 0003, ((struct dsg_track_entry)
                    {
                        .meta_event = dsg_meta_nothing | dsg_event_nochange,
                        .note_ctl   = 0,
                        .effect_1   = (uint8_t)(dsg_effect_volume | ((v & 0xF00) >> 8)),
                        .effect_2   = (uint8_t)(v & 0x0FF)
                    }));
                }
                else if ((cc & 0x70) && cv)
                {
                    uint8_t ch = cc & 0003;
                    uint16_t vs = 0x0000;
                    if (mdt->arps[ch])
                        mdt->arps[ch] = 0;
                    else if (!(cc & ~0x23) && (cc & 0x20) && ((ssize_t)cc - 0x20) < (ssize_t)player->config->chan_amt)
                    {
                        mdt->arps[ch] = arp_s;
                        vs = 0x0220;
                    }
                    else if (!(cc & ~0x33) && ((cc & 0x30) == 0x30) && ((ssize_t)cc - 0x30) < (ssize_t)player->config->chan_amt)
                    {
                        mdt->arps[ch] = arp_m;
                        vs = 0x0044;
                    }
                    else if (!(cc & ~0x43) && (cc & 0x40) && ((ssize_t)cc - 0x40) < (ssize_t)player->config->chan_amt)
                    {
                        mdt->arps[ch] = arp_r;
                        vs = 0x00C0;
                    }

                    dsg_player_upd_chan(player, ch, ((struct dsg_track_entry)
                    {
                        .meta_event = dsg_meta_nothing | dsg_event_nochange,
                        .note_ctl   = 0,
                        .effect_1   = ((vs & 0x0F00) >> 8) | dsg_effect_arpeggio,
                        .effect_2   =   vs & 0x00FF
                    }));

                }
            }
            break;
    }
}

static void* fe_midi_init(struct dsg_frontend fe)
{
    struct midi_init  id = *(struct midi_init*)fe.init_data;
    struct midi_data* ud =  (struct midi_data*)calloc(1, sizeof(struct midi_data));
    struct rtm_data * rd =  (struct rtm_data *)calloc(id.port_amt, sizeof(struct rtm_data));

    RtMidiInPtr* midi_ins = (RtMidiInPtr*)calloc(id.port_amt, sizeof(RtMidiInPtr));

    struct dsg_midi_port* ports = id.ports;

    for (uint32_t i = 0; i < id.port_amt; ++i)
    {
        RtMidiInPtr midi_in = rtmidi_in_create_default();

        printf("opening port %i\n", id.ports[i].portnum);
        rtmidi_open_port(midi_in, id.ports[i].portnum, rtmidi_get_port_name(midi_in, id.ports[i].portnum));

        rd[i] = (struct rtm_data){
            .portnum = i ,
            .data    = ud
        };

        rtmidi_in_set_callback(midi_in, rtm_callback, &rd[i]);
        rtmidi_in_ignore_types(midi_in, !ports[i].ignore_sysex, !ports[i].ignore_time, !ports[i].ignore_sense);

        midi_ins[i] = midi_in;
    }

    ud->midi_ins = midi_ins;
    ud->rtmd     = rd      ;
    ud->player   = NULL    ;

    return ud;
}
static struct dsg_player* fe_midi_mk_player(
    struct dsg_config* conf, void* data)
{
    struct midi_data* ud = (struct midi_data*)data;
    ud->arps = calloc(conf->chan_amt, sizeof(enum arp_type));

    return (ud->player = dsg_player_new_notrack(conf));
}
static bool fe_midi_run(
    struct dsg_config* conf, void* data, struct dsg_player* player
#ifndef DSG_DISABLE_FLOPPY
    , dsg_rpi_gpio gpio
    , volatile struct dsg_floppy_ctl* ctls
    , dsg_floppy_bg_thread* threads
#endif
    , struct dsg_handle_buffer_cb* buf_handlers)
{
#ifndef DSG_DISABLE_FLOPPY
    ((struct midi_data*)data)->bgt = threads;
#endif

    // TODO: copied from fe_track, should this be not a callback?
    dsg_player_play(player, false);
    struct timespec ts, te;
    while (player->playing)
    {
        clock_gettime(CLOCK_REALTIME, &ts);
      //dsg_player_step(player); // TODO: this doesn't work, MIDI-wise

        // TODO: add a callback for this, so buf_handlers
        // isn't exposed to the frontend?
        dsg_render_player(player, buf_handlers);
        clock_gettime(CLOCK_REALTIME, &te);

        // difference between ts and te
        int64_t dt = ((int64_t)te.tv_sec - ts.tv_sec) * NSEC_IN_SEC + ts.tv_nsec - te.tv_nsec;

        // planned time to pause
        // TODO: make this shorter, or timing bogosities might show up
        int64_t time = //(60 * (int64_t)NSEC_IN_SEC) / (int64_t)(player->tempo * player->time_div);
            (int64_t)(SAMPLE_RATE_RCP * NSEC_IN_SEC * (DEFAULT_BUF_SIZE - 0x1));
        // actual pause time
        time -= dt;

        if (time <= 0)
        {
            printf("W: tick took too long, continuing...\n");
            continue;
        }

        int64_t modNS = time % NSEC_IN_SEC;
        ts.tv_nsec = (ssize_t)modNS;
        ts.tv_sec  = (time_t)((time - modNS) / (int64_t)NSEC_IN_SEC);

        nanosleep(&ts, NULL);
    }

    return false;
}
static void fe_midi_kill(struct dsg_frontend fe, void* data)
{
    struct midi_data* ud = (struct midi_data*)data;

    for (size_t i = 0; i < ud->init->port_amt; ++i)
        rtmidi_in_free(ud->midi_ins[i]);

    ud->rtmd->data = NULL;

    free(ud->midi_ins);
    free(ud->rtmd    );

    ud->midi_ins = NULL;
    ud->rtmd     = NULL;

    free(ud          );

    struct midi_init* id = (struct midi_init*)fe.init_data;

    id->ports = NULL;

    free(id);
}

struct dsg_frontend dsg_make_midi_frontend(
    size_t port_amt, struct dsg_midi_port* ports)
{
    struct midi_init* id = (struct midi_init*)calloc(1, sizeof(struct midi_init));

    id->port_amt = port_amt;
    id->ports    = ports   ;

    struct dsg_frontend rev = (struct dsg_frontend)
    {
        .init_data = id,

        .init_fn      = fe_midi_init     ,
        .mk_player_fn = fe_midi_mk_player,
        .run_fn       = fe_midi_run      ,
        .kill_fn      = fe_midi_kill
    };

    return rev;
}

