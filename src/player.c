
#include <stdlib.h>

#include "player.h"

struct dsg_player* dsg_player_new(size_t rows, struct dsg_config* cfg, struct dsg_track_entry* entries)
{
    if (!rows || !cfg || !entries)
        return NULL;

    struct dsg_player* player = dsg_player_new_notrack(cfg);

    player->row_amt = rows   ;
    player->track   = entries;

    return player;
}
struct dsg_player* dsg_player_new_notrack(struct dsg_config* cfg)
{
    if (!cfg)
        return NULL;

    struct dsg_player* player = (struct dsg_player*)calloc(1, sizeof(struct dsg_player));

    player->row_amt  = 0;
    player->chan_amt = cfg->chan_amt;

    player->config   = cfg;
    player->tempo    = cfg->tempo;
    player->time_div = cfg->time_div;
    player->cur_row  = 0;

    player->track    = NULL;
    player->channels = (struct dsg_channel*)calloc(player->chan_amt, sizeof(struct dsg_channel));

    player->playing = false;

    for (size_t i = 0; i < player->chan_amt; ++i)
        dsg_channel_init(&player->channels[i], &cfg->chan_cfg[i]);

    return player;
}

void dsg_player_free(struct dsg_player* player)
{
    if (!player)
        return;

    for (size_t i = 0; i < player->chan_amt; ++i)
        dsg_channel_free(&player->channels[i]);

    if (player->channels)
        free(player->channels);

    free(player);
}

void dsg_player_play(struct dsg_player* player, bool force_chans)
{
    if (!player)
        return;

    player->playing = true;

    if (force_chans)
        for (size_t i = 0; i < player->chan_amt; ++i)
            dsg_channel_play(&player->channels[i]);
}

void dsg_player_step(struct dsg_player* player)
{
    if (!player || !player->playing || !player->row_amt || !player->track)
        return;

    if (player->tempo_del < 0 && (size_t)(-player->tempo_del) >= player->tempo)
    {
        player->playing = false;
        return;
    }
    player->tempo = (size_t)((int64_t)player->tempo + player->tempo_del);
    if (player->tempo * player->time_div >= 30 * SAMPLE_RATE) // 2 samples/buffer => ...
    {
        player->playing = false;
        return;
    }

    player->tempo_del = 0;

    size_t cur_row = player->cur_row;
    for (size_t i = 0; i < player->chan_amt; ++i)
    {
        struct dsg_track_entry e = dsg_player_get_entry(player, player->cur_row, i);

        if (dsg_get_meta(e) == dsg_meta_stop)
        {
            player->playing = false;
            return;
        }

        switch (dsg_get_effect_type(e))
        {
            case dsg_effect_tempo:
                if (dsg_get_effect_data_u(e))
                    player->tempo = dsg_get_effect_data_u(e);
                break;
            case dsg_effect_tempo_delta:
                player->tempo_del = dsg_get_effect_data_s(e);
                break;
            case dsg_effect_jump_rel:
                player->cur_row =
                    (size_t)((ssize_t)player->cur_row + dsg_get_effect_data_s(e)) % player->row_amt;
                break;
            case dsg_effect_jump_abs:
                player->cur_row = dsg_get_effect_data_u(e) % player->row_amt;
                break;
            default:
                break;
        }

        dsg_channel_upd(&player->channels[i], e);
    }

    if (cur_row == player->cur_row)
    {
        player->cur_row++;
        if (player->cur_row == player->row_amt)
            player->cur_row = 0;
    }
}

