
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#include "player.h"
#include "frontend.h"

#pragma clang diagnostic ignored "-Wunused-parameter"

struct fe_track_init_data
{
    struct dsg_track_entry* track;
    size_t rows;
};

static void* fe_track_init(struct dsg_frontend fe)
{
    return fe.init_data;
}
static struct dsg_player* fe_track_mk_player(
    struct dsg_config* conf, void* data)
{
    struct fe_track_init_data id = *(struct fe_track_init_data*)data;

    return dsg_player_new(id.rows, conf, id.track);
}
static bool fe_track_run(
    struct dsg_config* conf, void* data, struct dsg_player* player
#ifndef DSG_DISABLE_FLOPPY
    , dsg_rpi_gpio gpio
    , volatile struct dsg_floppy_ctl* ctls
    , dsg_floppy_bg_thread* threads
#endif
    , struct dsg_handle_buffer_cb* buf_handlers)
{
    dsg_player_play(player, false);
    struct timespec ts, te;
    while (player->playing)
    {
        clock_gettime(CLOCK_REALTIME, &ts);
        dsg_player_step(player);

        // TODO: add a callback for this, so buf_handlers
        // isn't exposed to the frontend?
        dsg_render_player(player, buf_handlers);
        clock_gettime(CLOCK_REALTIME, &te);

        // difference between ts and te
        int64_t dt = ((int64_t)te.tv_sec - ts.tv_sec) * NSEC_IN_SEC + ts.tv_nsec - te.tv_nsec;

        // planned time to pause
        int64_t time = (60 * (int64_t)NSEC_IN_SEC) / (int64_t)(player->tempo * player->time_div);
        // actual pause time
        time -= dt;

        if (time <= 0)
        {
            printf("W: tick took too long, continuing...\n");
            continue;
        }

        int64_t modNS = time % NSEC_IN_SEC;
        ts.tv_nsec = (ssize_t)modNS;
        ts.tv_sec  = (time_t)((time - modNS) / (int64_t)NSEC_IN_SEC);

        nanosleep(&ts, NULL);
    }

    return false;
}
static void fe_track_kill(struct dsg_frontend fe, void* data)
{
    free(data); // data == fe.init_data
}

struct dsg_frontend dsg_make_track_frontend(
    struct dsg_track_entry* track, size_t rows)
{
    struct fe_track_init_data* id =
        (struct fe_track_init_data*)calloc(1,
                sizeof(struct fe_track_init_data));

    id->track = track;
    id->rows  = rows ;

    struct dsg_frontend rev = (struct dsg_frontend)
    {
        .init_data = id,

        .init_fn      = fe_track_init     ,
        .mk_player_fn = fe_track_mk_player,
        .run_fn       = fe_track_run      ,
        .kill_fn      = fe_track_kill
    };

    return rev;
}

