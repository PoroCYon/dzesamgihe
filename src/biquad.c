
#include <math.h>

#include "fastmath.h"
#include "config.h"

#include "biquad.h"

float dsg_biquad_apply_sample(float sample,
    struct dsg_biquad_coeffs_norm* cns, struct dsg_biquad_state* state)
{
    if (!cns || !state)
        // TODO: decide whether to overwrite the state with NaNs so the
        // filter pops (and the error will thus be noticed)
        // (if the state isn't null, that is)
        return 0.0f / 0.0f;

    struct dsg_biquad_coeffs_norm cs = *cns;

    float sout = cs.c0 * sample + cs.c1 * state->prevx[1] + cs.c2 * state->prevx[0]
                                - cs.c3 * state->prevy[1] - cs.c4 * state->prevy[0];

    state->prevx[0] = state->prevx[1];
    state->prevy[0] = state->prevy[1];

    state->prevx[1] = sample;
    state->prevy[1] = sout  ;

    return sout;
}

void dsg_biquad_apply_buffer(size_t buf_len, float* buf,
    struct dsg_biquad_coeffs_norm* cns, struct dsg_biquad_state* state)
{
    if (!buf || !cns || !state)
        return;

    struct dsg_biquad_coeffs_norm cs = *cns  ;
    struct dsg_biquad_state        s = *state;

    float prevx0 = *buf,
          prevy0 = *buf =
              cs.c0 * prevx0 + cs.c1 * s.prevx[1] + cs.c2 * s.prevx[0]
                             - cs.c3 * s.prevy[1] - cs.c4 * s.prevy[0],
          prevx1 = buf[1],
          prevy1 = buf[1] =
              cs.c0 * prevx1 + cs.c1 * prevx0 + cs.c2 * s.prevx[1]
                             - cs.c3 * prevy0 - cs.c4 * s.prevy[1];

    for (size_t i = 2; i < buf_len; ++i)
    {
        float in = buf[i];
        buf[i] = cs.c0 * in + cs.c1 * prevx1 + cs.c2 * prevx0
                            - cs.c3 * prevy1 - cs.c3 * prevy0;

        prevx0 = prevx1;
        prevy0 = prevy1;

        prevx1 = in    ;
        prevy1 = buf[i];
    }

    state->prevx[0] = prevx0;
    state->prevx[1] = prevx1;

    state->prevy[0] = prevy0;
    state->prevy[1] = prevy1;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
struct dsg_biquad_coeffs dsg_biquad_mk_lowpass(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp,
          t = 1.0f - c;

    return (struct dsg_biquad_coeffs){
        .b0 =  t *  0.5f,
        .b1 =  t        ,
        .b2 =  t *  0.5f,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 = -a +  1.0f
    };
}
struct dsg_biquad_coeffs dsg_biquad_mk_highpass(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp,
          t = 1.0f + c;

    return (struct dsg_biquad_coeffs){
        .b0 =  t *  0.5f,
        .b1 = -t        ,
        .b2 =  t *  0.5f,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 =  a -  1.0f
    };
}

struct dsg_biquad_coeffs dsg_biquad_mk_bandpass_Q_peak_gain(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp;

    return (struct dsg_biquad_coeffs){
        .b0 =  s *  0.5f,
        .b1 =       0.0f,
        .b2 =  s * -0.5f,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 = -a +  1.0f,
    };
}
struct dsg_biquad_coeffs dsg_biquad_mk_bandpass_const_gain(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp;

    return (struct dsg_biquad_coeffs){
        .b0 =  a        ,
        .b1 =       0.0f,
        .b2 = -a        ,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 =  a -  1.0f
    };
}

struct dsg_biquad_coeffs dsg_biquad_mk_notch(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp;

    return (struct dsg_biquad_coeffs){
        .b0 =       1.0f,
        .b1 =  c * -2.0f,
        .b2 =       1.0f,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 = -a +  1.0f
    };
}
struct dsg_biquad_coeffs dsg_biquad_mk_allpass(float centre_freq, float gain, float Qrcp)
{
    float w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp;

    return (struct dsg_biquad_coeffs){
        .b0 = -a +  1.0f,
        .b1 =  c * -2.0f,
        .b2 =  a +  1.0f,
        .a0 =  a +  1.0f,
        .a1 =  c * -2.0f,
        .a2 = -a +  1.0f
    };
}

// V all these DO use the gain param
struct dsg_biquad_coeffs dsg_biquad_mk_peaking_EQ(float centre_freq, float gain, float Qrcp)
{
    float A = powf(10.0f, gain * 0.025f),
          w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          a = s * 0.5f * Qrcp;

    return (struct dsg_biquad_coeffs){
        .b0 =  a * A +  1.0f,
        .b1 =  c     * -2.0f,
        .b2 = -a * A +  1.0f,
        .a0 =  a * A +  1.0f,
        .a1 =  c     * -2.0f,
        .a2 = -a * A +  1.0f
    };
}

struct dsg_biquad_coeffs dsg_biquad_mk_low_shelf(float centre_freq, float gain, float Srcp)
{
    float A = powf(10.0f, gain * 0.025f),
          w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          t = s * sqrtf((A * A + 1.0f) * (Srcp - 1.0f) + 2.0f * A),
          M = A + 1.0f,
          m = A - 1.0f;

    return (struct dsg_biquad_coeffs){
        .b0 =         A * (M - m * c + t),
        .b1 =  2.0f * A * (m - M * c    ),
        .b2 =         A * (M - m * c - t),
        .a0 =              M + m * c + t ,
        .a1 = -2.0f     * (m + M * c    ),
        .a2 =              M + m * c - t
    };
}
struct dsg_biquad_coeffs dsg_biquad_mk_high_shelf(float centre_freq, float gain, float Srcp)
{
    float A = powf(10.0f, gain * 0.025f),
          w = TAU_F * centre_freq * SAMPLE_RATE_RCP,
          c = fast_cos(w),
          s = fast_sin(w),
          t = s * sqrtf((A * A + 1.0f) * (Srcp - 1.0f) + 2.0f * A),
          M = A + 1.0f,
          m = A - 1.0f;

    return (struct dsg_biquad_coeffs){
        .b0 =         A * (M + m * c + t),
        .b1 = -2.0f * A * (m + M * c    ),
        .b2 =         A * (M + m * c - t),
        .a0 =              M - m * c + t ,
        .a1 =  2.0f *     (m - M * c    ),
        .a2 =              M - m * c - t
    };
}
#pragma clang diagnostic pop

