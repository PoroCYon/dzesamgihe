
#ifdef DSG_DISABLE_FLOPPY
#pragma clang diagnostic ignored "-Wempty-translation-unit"
#warning "GPIO support disabled."
#else
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "gpio.h"

#define NUM_SYSFS (40)

#define PAGE_SIZE (0x1000)

struct dsg_rpi_mmap
{
    volatile size_t* map;
    int filed;
};
struct dsg_rpi_sysfs
{
    int fileds[NUM_SYSFS];
};

union dsg_rpi_handles
{
    struct dsg_rpi_mmap  mmap ;
    struct dsg_rpi_sysfs sysfs;
};

struct dsg_rpi
{
    struct dsg_rpi_board_id board_id;

    union dsg_rpi_handles handles;

    enum dsg_gpio_mode mode;
};

struct dsg_rpi_board_id dsg_rpi_get_board_id(void)
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wassign-enum"
    static struct dsg_rpi_board_id fail = (struct dsg_rpi_board_id){
        .model      = ~(enum dsg_rpi_model   )0,
        .revision   = ~(enum dsg_rpi_revision)0,
      //.new_layout = false
    };
#pragma clang diagnostic pop

    const static size_t linebuf_len = 0x100;
    static char linebuf[0x100];

    static char hardwares[8] = "Hardware",
                revisions[8] = "Revision";
    static size_t hwlen = 8, rvlen = 8;

  //static char* bcmboards[3] = { "BCM2708", "BCM2709", "BCM2835" };

  //static char *revs2 = "0002",
  //            *revs3 = "0003";

    enum dsg_rpi_model    model   ;
    enum dsg_rpi_revision revision;
  //bool new_layout;

    FILE* fh = fopen("/proc/cpuinfo", "r");
    if (!fh)
        return fail;

    {
        while (fgets(linebuf, linebuf_len, fh) && strncmp(linebuf, hardwares, hwlen));

        if (strncmp(linebuf, hardwares, hwlen))
            return fail;
      /*if (!(strstr(line, bcmboards[0]) || strstr(line, bcmboards[1]) || strstr(line, bcmboards[2])))
            return fail;*/
    }

    rewind(fh);

    {
        while (fgets(linebuf, linebuf_len, fh) && strncmp(linebuf, revisions, rvlen)) ;

        fclose(fh);

        if (strncmp(linebuf, revisions, rvlen))
            return fail;

        char* c;
        for (c = &linebuf[strlen(linebuf) - 1]; *c == '\n' || *c == '\r'; --c)
            *c = '\0';
        for (c = linebuf; *c && *c != ':'; ++c) ;
        if (*c != ':')
            return fail;
        do { ++c; } while (isspace(*c));
        if (!isxdigit(*c) || strlen(c) < 4)
            return fail;

        size_t revv = (size_t)strtol(c, NULL, 16);

        if (revv & (1 << 23))
        {
            size_t revn = (revv & (0x0F <<  0)) >>  0,
                   type = (revv & (0xFF <<  4)) >>  4/*,
                   proc = (revv & (0x0F << 12)) >> 12,
                   mfg  = (revv & (0x0F << 16)) >> 16,
                   mem  = (revv & (0x07 << 20)) >> 20,
                   ovv  = (revv & (0x03 << 24))      */;

            model    = (enum dsg_rpi_model   )type;
            revision = (enum dsg_rpi_revision)revn;
        }
        else
        {
            c += strlen(c) - 4;

            size_t modv = (size_t)strtol(c, NULL, 16);

            switch (modv)
            {
                case 0x0007: case 0x0008: case 0x0009:
                    model = dsg_rpi_a;
                    break;
                case 0x0002: case 0x0003:
                case 0x0004: case 0x0005: case 0x0006:
                case 0x000D: case 0x000E: case 0x000F:
                    model = dsg_rpi_b;
                    break;
                case 0x0012: case 0x0015: case 0x0018: case 0x001B:
                    model = dsg_rpi_aplus;
                    break;
                case 0x0010: case 0x0013: case 0x0016: case 0x0019:
                    model = dsg_rpi_bplus;
                    break;
                case 0x0011: case 0x0014: case 0x0017: case 0x001A:
                    model = dsg_rpi_cm;
                    break;
                default:
                    model = 0;
                    break;
            }
            switch (modv)
            {
                case 0x0002:
                    revision = dsg_rpi_rev_1;
                    break;
                case 0x0003:
                case 0x0011: case 0x0014: case 0x0017: case 0x001A:
                case 0x0012: case 0x0015: case 0x0018: case 0x001B:
                    revision = dsg_rpi_rev_1_1;
                    break;
                case 0x0004: case 0x0005: case 0x0006:
                case 0x0007: case 0x0008: case 0x0009:
                case 0x000D: case 0x000E: case 0x000F:
                case 0x0010: case 0x0013: case 0x0016: case 0x0019:
                    revision = dsg_rpi_rev_1_2;
                    break;
                default:
                    revision = 0;
                    break;
            }
        }

      //new_layout = strcmp(c, revs2) && strcmp(c, revs3);

        return (struct dsg_rpi_board_id){
            .model      = model     ,
            .revision   = revision  ,
          //.new_layout = new_layout
        };
    }
}

static size_t get_gpio_base(struct dsg_rpi_board_id board_id)
{
    size_t peri_base;
    switch (board_id.model)
    {
        case dsg_rpi_a    : case dsg_rpi_b    :
        case dsg_rpi_aplus: case dsg_rpi_bplus:
        case dsg_rpi_alpha: case dsg_rpi_cm   :
        case dsg_rpi_zero :
            peri_base = 0x20000000;
            break;
        default:
            peri_base = 0x3F000000;
            break;
    }

    return peri_base + 0x00200000;
}
static dsg_rpi_gpio dsg_rpi_init_mmap(
    char* filep, enum dsg_gpio_mode mode, struct dsg_rpi_board_id board_id)
{
    size_t gpio_base = get_gpio_base(board_id);

    int filed = open(filep, O_RDWR|O_SYNC);

    if (filed == -1)
        return NULL;
    void* map = mmap(NULL, PAGE_SIZE, PROT_WRITE|PROT_READ, MAP_SHARED, filed, (off_t)gpio_base);
    if (!map || map == MAP_FAILED)
        return NULL;

    struct dsg_rpi* ret = calloc(1, sizeof(struct dsg_rpi));

    ret->mode     = mode    ;
    ret->board_id = board_id;
    ret->handles.mmap = (struct dsg_rpi_mmap){
        .map   = (volatile size_t*)map,
        .filed = filed
    };

    return ret;
}
static dsg_rpi_gpio dsg_rpi_init_int(
    enum dsg_gpio_mode mode, struct dsg_rpi_board_id board_id)
{
    switch (mode)
    {
        case dsg_gpio_mmap_gpiomem:
            if (access("/dev/gpiomem", R_OK|W_OK))
                return dsg_rpi_init_int(dsg_gpio_mmap_mem, board_id);

            {
                dsg_rpi_gpio r = dsg_rpi_init_mmap("/dev/gpiomem", mode, board_id);

                if (r) return r;

                return dsg_rpi_init_int(dsg_gpio_mmap_mem, board_id);
            }
        case dsg_gpio_mmap_mem:
            if (getuid() /* requires root */
                    || access("/dev/mem", R_OK|W_OK))
                return dsg_rpi_init_int(dsg_gpio_sysfs, board_id);

            {
                dsg_rpi_gpio r = dsg_rpi_init_mmap("/dev/mem", mode, board_id);

                if (r) return r;

                return dsg_rpi_init_int(dsg_gpio_sysfs, board_id);
            }
        case dsg_gpio_sysfs:
            if (access("/sys/class/gpio/export", W_OK))
                return NULL;

            {
                struct dsg_rpi_sysfs sysfs;
                memset(sysfs.fileds, 0x00, sizeof(int) * NUM_SYSFS);

                int filed = open("/sys/class/gpio/export", O_WRONLY|O_SYNC);
                if (filed == -1)
                    return NULL;

                for (size_t pin = 0; pin < NUM_SYSFS; ++pin)
                {
                    char buf[8];
                    sprintf(buf, "%zu\n", pin);

                    if (write(filed, buf, strlen(buf)) == -1)
                    {
                        for (size_t p = 0; p < pin; ++p) if (sysfs.fileds[p] != -1) close(sysfs.fileds[p]);

                        close(filed);

                        return NULL;
                    }

                    char fnameb[0x80];
                    sprintf(fnameb, "/sys/class/gpio/gpio%zu/value", pin);

                    // file doesn't exist -> reached the end
                    if (access(fnameb, F_OK))
                    {
                        // fill the nonexistent ones with invalid filedescriptors
                        for (size_t p = pin; p < NUM_SYSFS; ++p)
                            sysfs.fileds[p] = -1;

                        int filed_ = open("/sys/class/gpio/unexport", O_WRONLY|O_SYNC);
                        if (filed_ == -1)
                        {
                            for (size_t p = 0; p < pin; ++p) if (sysfs.fileds[p] != -1) close(sysfs.fileds[p]);

                            close(filed);

                            return NULL;
                        }

                        // unexport it, though
                        if (write(filed_, buf, strlen(buf)) == -1)
                        {
                            for (size_t p = 0; p < pin; ++p) if (sysfs.fileds[p] != -1) close(sysfs.fileds[p]);

                            close(filed_);
                            close(filed );

                            return NULL;
                        }

                        close(filed_);
                        break;
                    }
                    // file exists but can't rw -> something's wrong
                    if (access(fnameb, R_OK|W_OK))
                    {
                        for (size_t p = 0; p < pin; ++p) if (sysfs.fileds[p] != -1) close(sysfs.fileds[p]);

                        close(filed);

                        return NULL;
                    }

                    sysfs.fileds[pin] = open(fnameb, O_RDWR|O_SYNC);
                }

                close(filed); // not needed anymore

                struct dsg_rpi* ret = calloc(1, sizeof(struct dsg_rpi));

                ret->mode     = mode    ;
                ret->board_id = board_id;
                ret->handles.sysfs = sysfs;

                return ret;
            }
        default:
            return NULL;
    }
}
dsg_rpi_gpio dsg_rpi_init(enum dsg_gpio_mode preferred_mode)
{
    return dsg_rpi_init_int(preferred_mode, dsg_rpi_get_board_id());
}

void dsg_rpi_kill(dsg_rpi_gpio gpio)
{
    struct dsg_rpi* rpi = (struct dsg_rpi*)gpio;

    switch (rpi->mode)
    {
        case dsg_gpio_sysfs:
            {
                int* fileds = rpi->handles.sysfs.fileds;

                int filed = open("/sys/class/gpio/unexport", O_WRONLY|O_SYNC);

                char buf[8];

                for (size_t pin = 0; pin < NUM_SYSFS && fileds[pin] != -1; ++pin)
                {
                    if (filed != -1)
                    {
                        sprintf(buf, "%zu\n", pin);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-result"
                        write(filed, buf,strlen(buf));
#pragma clang diagnostic pop
                    }

                    close(fileds[pin]);
                }

                if (filed != -1)
                    close(filed);
            }
            break;
        default:
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"
            munmap((void*)rpi->handles.mmap.map, PAGE_SIZE);
#pragma clang diagnostic pop
            close(rpi->handles.mmap.filed);
            break;
    }
}

void dsg_gpio_set_pin_dir(dsg_rpi_gpio gpio, size_t pin, enum dsg_gpio_pin_dir dir)
{
    if (!~pin)
        return;

    static char * in_str =  "in\n",
                *out_str = "out\n";

    struct dsg_rpi* rpi = (struct dsg_rpi*)gpio;

    switch (rpi->mode)
    {
        case dsg_gpio_sysfs:
            if (pin >= NUM_SYSFS)
                return;

            {
                char* dstr = (dir == dsg_gpio_pin_input) ? in_str : out_str;

                int filed = rpi->handles.sysfs.fileds[pin];

                if (filed != -1)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-result"
                    write(filed, dstr, strlen(dstr));
#pragma clang diagnostic pop
            }
            break;
        default:
            {
                volatile size_t* gpiop = rpi->handles.mmap.map;
                size_t fsel  = (pin - (pin % 10) /* do round down */) / 10,
                       shift = (pin % 10) * 3;

                if (dir == dsg_gpio_pin_input)
                    *(gpiop + fsel) &= ~((size_t)7 << shift);
                else
                    *(gpiop + fsel) = (*(gpiop + fsel) & ~((size_t)7 << shift)) | ((size_t)1 << shift);
            }
            break;
    }
}

void dsg_gpio_set_pin(dsg_rpi_gpio gpio, size_t pin, bool val)
{
    if (!~pin)
        return;

    static char *hi_str = "1\n",
                *lo_str = "0\n";

    struct dsg_rpi* rpi = (struct dsg_rpi*)gpio;

    switch (rpi->mode)
    {
        case dsg_gpio_sysfs:
            if (pin >= NUM_SYSFS)
                return;

            {
                char* dstr = val ? hi_str : lo_str;

                int filed = rpi->handles.sysfs.fileds[pin];

                if (filed != -1)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-result"
                    write(filed, dstr, strlen(dstr));
#pragma clang diagnostic pop
            }
            break;
        default:
            {
                volatile size_t* gpiop = rpi->handles.mmap.map;

                size_t off = (val ? 7 : 10) + (pin >> 5);

                *(gpiop + off) = 1 << (pin & 0x1F);
            }
    }
}
bool dsg_gpio_get_pin(dsg_rpi_gpio gpio, size_t pin)
{
    if (!~pin)
        return false;

    struct dsg_rpi* rpi = (struct dsg_rpi*)gpio;

    switch (rpi->mode)
    {
        case dsg_gpio_sysfs:
            if (pin >= NUM_SYSFS)
                return false;

            {
                int filed = rpi->handles.sysfs.fileds[pin];

                if (filed == -1)
                    return false;

                char buf[2];
                if (read(filed, buf, 2) == -1)
                    return false;

                return buf[0] != '0';
            }
        default:
            {
                volatile size_t* gpiop = rpi->handles.mmap.map;
                size_t lev = 13 + (pin >> 5);

                return (*(gpiop + lev) & (1 << (pin & 0x1F))) != 0;
            }
    }
}

size_t dsg_rpi_convert_pin(
    /*dsg_rpi_gpio gpio,*/ size_t pin, enum dsg_gpio_pin_type from, enum dsg_gpio_pin_type to)
{
    if (from == to)
        return pin;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
    // pin numbers from pinout.xyz
    switch (from)
    {
        case dsg_gpio_pin_physical: // TODO: decide whether physical pins should be 0- or 1-based
            switch (to)             // TODO: (it's currently 1)
            {   case dsg_gpio_pin_broadcom:
                    switch (pin)
                    {   case  3: return  2;
                        case  5: return  3;
                        case  7: return  4;
                        case  8: return 14;
                        case 10: return 15;
                        case 11: return 17;
                        case 12: return 18;
                        case 13: return 27;
                        case 15: return 22;
                        case 16: return 23;
                        case 18: return 24;
                        case 19: return 10;
                        case 21: return  9;
                        case 22: return 25;
                        case 23: return 11;
                        case 24: return  8;
                        case 26: return  7;
                        case 27: return  0;
                        case 28: return  1;
                        case 29: return  5;
                        case 31: return  6;
                        case 32: return 12;
                        case 33: return 13;
                        case 35: return 19;
                        case 36: return 16;
                        case 37: return 26;
                        case 38: return 20;
                        case 40: return 21;
                    } break;
                case dsg_gpio_pin_wiringpi:
                    switch (pin)
                    {   case  3: return  8;
                        case  5: return  9;
                        case  7: return  7;
                        case  8: return 15;
                        case 10: return 16;
                        case 11: return  0;
                        case 12: return  1;
                        case 13: return  2;
                        case 15: return  3;
                        case 16: return  4;
                        case 18: return  5;
                        case 19: return 12;
                        case 21: return 13;
                        case 22: return  6;
                        case 23: return 14;
                        case 24: return 10;
                        case 26: return 11;
                        case 27: return 30;
                        case 28: return 31;
                        case 29: return 21;
                        case 31: return 22;
                        case 32: return 26;
                        case 33: return 23;
                        case 35: return 24;
                        case 36: return 27;
                        case 37: return 25;
                        case 38: return 28;
                        case 40: return 29;
                    } break;
            } break;
        case dsg_gpio_pin_broadcom:
            switch (to)
            {   case dsg_gpio_pin_physical:
                    switch (pin)
                    {   case  0: return 27;
                        case  1: return 28;
                        case  2: return  3;
                        case  3: return  5;
                        case  4: return  7;
                        case  5: return 29;
                        case  6: return 31;
                        case  7: return 26;
                        case  8: return 24;
                        case  9: return 21;
                        case 10: return 19;
                        case 11: return 23;
                        case 12: return 32;
                        case 13: return 33;
                        case 14: return  8;
                        case 15: return 10;
                        case 16: return 36;
                        case 17: return 11;
                        case 18: return 12;
                        case 19: return 35;
                        case 20: return 38;
                        case 21: return 40;
                        case 22: return 15;
                        case 23: return 16;
                        case 24: return 18;
                        case 25: return 22;
                        case 26: return 37;
                        case 27: return 13;
                    } break;
                case dsg_gpio_pin_wiringpi:
                    switch (pin)
                    {   case  0: return 30;
                        case  1: return 31;
                        case  2: return  8;
                        case  3: return  9;
                        case  4: return  7;
                        case  5: return 21;
                        case  6: return 22;
                        case  7: return 11;
                        case  8: return 10;
                        case  9: return 13;
                        case 10: return 12;
                        case 11: return 14;
                        case 12: return 26;
                        case 13: return 23;
                        case 14: return 15;
                        case 15: return 16;
                        case 16: return 27;
                        case 17: return  0;
                        case 18: return  1;
                        case 19: return 24;
                        case 20: return 28;
                        case 21: return 29;
                        case 22: return  3;
                        case 23: return  4;
                        case 24: return  5;
                        case 25: return  6;
                        case 26: return 25;
                        case 27: return  2;
                    } break;
            } break;
        case dsg_gpio_pin_wiringpi:
            switch (to)
            {   case dsg_gpio_pin_physical:
                    switch (pin)
                    {   case  0: return 11;
                        case  1: return 12;
                        case  2: return 13;
                        case  3: return 15;
                        case  4: return 16;
                        case  5: return 18;
                        case  6: return 22;
                        case  7: return  7;
                        case  8: return  3;
                        case  9: return  5;
                        case 10: return 24;
                        case 11: return 26;
                        case 12: return 19;
                        case 13: return 21;
                        case 14: return 23;
                        case 15: return  8;
                        case 16: return 10;
                        case 21: return 29;
                        case 22: return 31;
                        case 23: return 33;
                        case 24: return 35;
                        case 25: return 37;
                        case 26: return 32;
                        case 27: return 36;
                        case 28: return 38;
                        case 29: return 40;
                        case 30: return 27;
                        case 31: return 28;
                    } break;
                case dsg_gpio_pin_broadcom:
                    switch (pin)
                    {   case  0: return 17;
                        case  1: return 18;
                        case  2: return 27;
                        case  3: return 22;
                        case  4: return 23;
                        case  5: return 24;
                        case  6: return 25;
                        case  7: return  4;
                        case  8: return  2;
                        case  9: return  3;
                        case 10: return  8;
                        case 11: return  7;
                        case 12: return 10;
                        case 13: return  9;
                        case 14: return 11;
                        case 15: return 14;
                        case 16: return 15;
                        case 21: return  5;
                        case 22: return  6;
                        case 23: return 13;
                        case 24: return 19;
                        case 25: return 26;
                        case 26: return 12;
                        case 27: return 16;
                        case 28: return 20;
                        case 29: return 21;
                        case 30: return  0;
                        case 31: return  1;
                    } break;
            } break;
    }
#pragma clang diagnostic pop

    return ~(size_t)0;
}

#endif

