
#include <math.h>
#include <stdint.h>

#include "fastmath.h"

size_t min(size_t a, size_t b)
{
    return (a > b) ? b : a;
}
size_t max(size_t a, size_t b)
{
    return (a > b) ? a : b;
}

int fast_sign_i(int x)
{
    return (x > 0) - (x < 0);
}
int fast_sign_f(float x)
{
    return (int)((x > 0.0f) - (x < 0.0f));
}
int fast_sign_d(double x)
{
    return (int)((x > 0.0) - (x < 0.0));
}

size_t gcd(size_t a, size_t b)
{
    if (!a)
        return b;
    if (!b)
        return a;

    // throw away common multiples of 2
    size_t i;
    for (i = 0; !((a | b) & 1); ++i)
    {
        a >>= 1;
        b >>= 1;
    }

    // make a mod 2 = 1
    while (!(a & 1))
        a >>= 1;

    do
    {
        while (!(b & 1))
            b >>= 1;

        if (a > b)
        {
            size_t t = a;
            a = b;
            b = t;
        }

        b -= a;
    } while (b);

    return a << i;
}
size_t lcm(size_t a, size_t b)
{
    return a * (b / gcd(a, b));
}

float wrap_angle(float x)
{
    if (x >= -PI_F && x <= PI_F)
        return x;

    float v = remainderf(x, (float)TAU_D);

    if (v >  PI_F)
        return v - TAU_F;
    //if (v < -PI_F)
    //    return v + TAU_F;

    return v;
}
double wrap_angle_d(double x)
{
    if (x >= -PI_D && x <= PI_D)
        return x;

    double v = remainder(x, TAU_D);

    if (v >  PI_D)
        return v - TAU_D;
    //if (v < -PI_D)
    //    return v + TAU_D;

    return v;
}

static float fast_pow_help(float x, int64_t exp)
{
    // if n % m == 0:
    // x^n = x^(n/m * m) = (x^(n/m))^m

    if (exp & 1L)
    {
        // only checking 2 & 3 should be good enough
        if (!(exp % 3L))
        {
            float v = fast_pow_help(x, exp / 3L);
            return v * v * v;
        }

        for (int64_t i = 0L; i < exp; ++i, x *= x) ;

        return x;
    }

    float v = fast_pow_help(x, exp >> 1);
    return v * v;
}
float fast_pow(float x, int64_t exp)
{
    if (!exp)
        return 1.0f;
    if (exp < 0L)
        return 1.0f / fast_pow_help(x, -exp);

    return fast_pow_help(x, exp);
}

static double fast_pow_d_help(double x, int64_t exp)
{
    if (exp & 1L)
    {
        if (!(exp % 3L))
        {
            double v = fast_pow_d_help(x, exp / 3L);
            return v * v * v;
        }

        for (int64_t i = 0L; i < exp; ++i, x *= x) ;

        return x;
    }

    double v = fast_pow_d_help(x, exp >> 1);
    return v * v;
}
double fast_pow_d(double x, int64_t exp)
{
    if (!exp)
        return 1.0;
    if (exp < 0L)
        return 1.0 / fast_pow_d_help(x, -exp);

    return fast_pow_d_help(x, exp);
}

float fast_inv_sqrt(float x)
{
    // if you don't recognise this one, read more coder lore

    int32_t i = *(int32_t*)&x;
    i = 0x5F3759DF - (i >> 1);

    float y = *(float*)&i;

    return y * (1.5f - (x * 0.5f * y * y));
}

int fast_log2_u8(uint8_t b)
{
    if (!b)
        return -1;

    if (b & 0xF0)
    {
        for (int i = 7; i > 4; --i)
            if (b & (1 << i))
                return i;

        return 0; // this shouldn't happen (see 'if (b & 0xF0)')
    }

    for (int i = 0; i < 5; ++i)
        if (b & (1 << i))
            return i;

    return -1; // this shouldn't happen (see 'if (!b)')
}
int fast_log2_u16(uint16_t s)
{
    if (!s)
        return -1;

    if (s & 0xFF00)
        return fast_log2_u8((uint8_t)(s >> 0x08)) + 0x08;

    return fast_log2_u8((uint8_t)s);
}
int fast_log2_u32(uint32_t u)
{
    if (!u)
        return -1;

    if (u & 0xFFFF0000)
        return fast_log2_u16((uint16_t)(u >> 0x10)) + 0x10;

    return fast_log2_u16((uint16_t)u);
}
int fast_log2_u64(uint64_t l)
{
    if (!l)
        return -1;

    if (l & 0xFFFFFFFF00000000)
        return fast_log2_u32((uint32_t)(l >> 0x20)) + 0x20;

    return fast_log2_u32((uint32_t)l);
}
int fast_log2_i8(int8_t c)
{
    if (c < 1)
        return -1;

    return fast_log2_u8((uint8_t)c);
}
int fast_log2_i16(int16_t s)
{
    if (s < 1)
        return -1;

    return fast_log2_u16((uint16_t)s);
}
int fast_log2_i32(int32_t i)
{
    if (i < 1)
        return -1;

    return fast_log2_u32((uint32_t)i);
}
int fast_log2_i64(int64_t l)
{
    if (l < 1L)
        return -1;

    return fast_log2_u64((uint64_t)l);
}

// code blatantly ripped off from Farbrausch (but public domain)
// https://github.com/farbrausch/fr_public/blob/master/v2/synth.asm

float fast_atan(float x)
{
    float x2 = x * x;

    return
        (0.4315794f  * x2 * x  + x                      ) /
        (0.05831938f * x2 * x2 + 0.76443945f * x2 + 1.0f);
}
double fast_atan_d(double x)
{
    double x2 = x * x;

    return
        (0.4315794  * x2 * x  + x                    ) /
        (0.05831938 * x2 * x2 + 0.76443945 * x2 + 1.0);
}

float fast_atan2(float y, float x)
{
    if (x == 0.0f)
        return HALF_PI_F;

    return fast_atan(y / x);
}
double fast_atan2_d(double y, double x)
{
    if (x == 0.0)
        return HALF_PI_D;

    return fast_atan_d(y / x);
}

float fast_atan_inv(float x)
{
    float x2 = x * x;

    return
        (x2 * x  + 0.431597974f * x               ) /
        (x2 * x2 + 0.76443945f  * x2 + 0.05831938f);
}
double fast_atan_inv_d(double x)
{
    double x2 = x * x;

    return
        (x2 * x  + 0.431597974 * x              ) /
        (x2 * x2 + 0.76443945  * x2 + 0.05831938);
}

float fast_sin(float x)
{
    float x2 = x * x;

    return (
            (
             (
              (-0.00018542f * x2)
               + 0.0083143f)
             * x2 - 0.16666f)
            * x2 + 1)
           * x;
}
double fast_sin_d(double x)
{
    double x2 = x * x;

    return (
            (
             (
              (-0.00018542 * x2)
               + 0.0083143)
             * x2 - 0.16666)
            * x2 + 1)
           * x;
}

float fast_cos(float x)
{
    // if v = 0, return 1
    // if v < 0, use -sin (x + pi/2)
    // if v > 0, use  sin (x - pi/2)
    int s = fast_sign_f(x);

    if (!s)
        return 1.0f;

    return (float)s * fast_sin(x - (float)s * HALF_PI_F);
}
double fast_cos_d(double x)
{
    // if v = 0, return 1
    // if v < 0, use -sin (x + pi/2)
    // if v > 0, use  sin (x - pi/2)
    int s = fast_sign_d(x);

    if (!s)
        return 1.0;

    return (double)s * fast_sin_d(x - (double)s * HALF_PI_D);
}
