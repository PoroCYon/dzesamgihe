
#include "chan.h"
#include "oscs.h"

void dsg_channel_init(struct dsg_channel* chan, struct dsg_cfg_chan* ccfg)
{
    if (!chan || !ccfg)
        return;

    chan->base_note = 0;

    chan->cur_effect = 0x00;

    chan->duty_cycle  = 0.5f ;
    chan->duty_sweep  = 0.0f ;
    chan->pitch_slide = 0.0f ;
    chan->arpeggio    = 0x000;
    chan->arp_speed   = 1    ;

    chan->muted   = false;
    chan->playing = false;

    chan->time_total  = 0   ;
    chan->type        = (enum dsg_chan_type)ccfg->type;
    chan->extra_pitch = 0.0f;

    chan->volume      = 1.0f;
    chan->volume_temp = 1.0f;
    chan->volume_del  = 0.0f;

    chan->harmony     = 0.0f;
    chan->h_amplitude = 1.0f;

    chan->oscillator = dsg_osc_from_type(ccfg->type);
}

void dsg_channel_play(struct dsg_channel* chan)
{
    if (chan)
        chan->playing = true;
}

void dsg_channel_upd(struct dsg_channel* chan, struct dsg_track_entry cur_entry)
{
    if (!chan)
        return;

    switch (dsg_get_meta(cur_entry))
    {
        case dsg_meta_nothing:
            break;
        case dsg_meta_mute:
            chan->muted = true;
            break;
        case dsg_meta_unmute:
            chan->muted = false;
            break;
        default:
            break;
    }

    switch (dsg_get_event(cur_entry))
    {
        case dsg_event_nochange:
            break;
        case dsg_event_note_on:
            chan->playing     = true;
            chan->base_note   = dsg_get_note(cur_entry);
          //chan->extra_pitch = 0.0f;
            chan->volume_temp = 1.0f;
            break;
        case dsg_event_note_off:
            chan->volume_temp = 1.0f;
          //chan->extra_pitch = 0.0f;
            chan->playing     = false;
            break;
        case dsg_event_control:
            // TODO: add control stuff (when added)
            break;
    }

    switch (chan->cur_effect = dsg_get_effect_type(cur_entry))
    {
        case dsg_effect_none:
            break;
        case dsg_effect_duty_sweep:
            if (chan->type == dsg_chan_soft_fm)
                chan->h_amplitude = (float)(int32_t)dsg_get_effect_data_u(cur_entry) * (1.0f / 4095.0f);
            else
                chan->duty_sweep = (float)(int32_t)dsg_get_effect_data_u(cur_entry) * (1.0f / 4095.0f);
            break;
        case dsg_effect_duty_cycle:
            if (chan->type == dsg_chan_soft_fm)
                chan->harmony = (float)(int32_t)dsg_get_effect_data_u(cur_entry);
            else
                chan->duty_cycle = (float)(int32_t)dsg_get_effect_data_u(cur_entry) * (1.0f / 4095.0f);
            break;
        case dsg_effect_arpeggio:
            chan->arpeggio = dsg_get_effect_data_u(cur_entry);
            break;
        case dsg_effect_arps_td_biq:
            {
                uint16_t data = dsg_get_effect_data_u(cur_entry);

                uint8_t arps = (uint8_t)((data & 0x0F00) >> 8);

                if (arps != 0x0F)
                {
                    int8_t arps_ = (int8_t)(arps & 0x07);
                    if (arps & 0x08)
                        arps_ = -arps_;

                    chan->arp_speed = arps_ + 1;
                }
            }
            break;
        case dsg_effect_pitch_slide:
            chan->pitch_slide = (float)(int32_t)dsg_get_effect_data_s(cur_entry);
            break;
        case dsg_effect_volume:
            {
                static const float divv = 1.0f / 4095.0f;
                chan->volume = dsg_get_effect_data_u(cur_entry) * divv;
            }
            break;
        case dsg_effect_volume_temp:
            {
                static const float divv = 1.0f / 4095.0f;
                chan->volume_temp = dsg_get_effect_data_u(cur_entry) * divv;
            }
            break;
        case dsg_effect_volume_del:
            {
                static const float divv = 1.0f / (4095.0f * 0x80);

                chan->volume_del = dsg_get_effect_data_s(cur_entry) * divv;
            }
            break;
        case dsg_effect_extra_pitch:
            {
                static const float divv = 1.0f / 8.0f;

                chan->extra_pitch = dsg_get_effect_data_s(cur_entry) * divv;
            }
            break;
        default:
            break;
    }
}

void dsg_channel_free(struct dsg_channel* chan)
{
    if (chan && chan->oscillator.state)
        dsg_osc_free_state(chan->oscillator.state);
}

