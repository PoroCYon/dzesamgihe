
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "bufqueue.h"

#pragma clang diagnostic ignored "-Wcast-qual"

void dsg_buf_enqueue(volatile struct dsg_buf_queue*volatile* q,
        size_t buf_size, float* buf, bool no_copy)
{
    if (!q || !buf_size || !buf) return;

    struct dsg_buf bufv = (struct dsg_buf){
        .buf     = buf     ,
        .size    = buf_size,
        .is_copy = !no_copy
    };

    if (bufv.is_copy)
    {
        bufv.buf = (float*)calloc(buf_size, sizeof(float));
        memcpy(bufv.buf, buf, buf_size * sizeof(float));
    }

    volatile struct dsg_buf_queue* qq = *q;
    if (!qq)
    {
        qq = *q = (volatile struct dsg_buf_queue*)
            calloc(1, sizeof(struct dsg_buf_queue));

        qq->next = NULL;
        qq->buf  = bufv;

        return;
    }

    while (qq->next)
        qq = qq->next;

    qq->next = (volatile struct dsg_buf_queue*)
        calloc(1, sizeof(struct dsg_buf_queue));

    qq = qq->next;

    qq->next = NULL;
    qq->buf  = bufv;

    return;
}

struct dsg_buf dsg_buf_dequeue(volatile struct dsg_buf_queue*volatile* q)
{
    static struct dsg_buf fail = (struct dsg_buf){
        .buf     = NULL ,
        .size    = 0    ,
        .is_copy = false
    };

    if (!q || !(*q)) return fail;

    struct dsg_buf retv = (*q)->buf;

    *q = (*q)->next;

    return retv;
}

void dsg_buf_free(volatile struct dsg_buf_queue* q, bool recurse)
{
    while (q)
    {
        if (q->buf.is_copy)
            free(q->buf.buf);

        volatile struct dsg_buf_queue* q_ = q->next;

        free((void*)q);

        if (recurse) q = q_;
        else return;
    }
}

void dsg_buf_print(volatile struct dsg_buf_queue* q, size_t cookie)
{
    size_t i = 0;

    while (q)
    {
        printf("%zu: %zu %p - %zu %p %i\n", cookie, i, (void*)q, q->buf.size, (void*)q->buf.buf, q->buf.is_copy);

        i++;
        q = q->next;
    }
}

