
#define _GNU_SOURCE
#include <stdlib.h>
#include <memory.h>
#include <time.h>
#include <sys/time.h>

#include "fastmath.h"
#include "osc.h"
#include "oscs.h"

struct dsg_osc_phase
{
    float phase;
};
struct dsg_osc_noise
{
    size_t x;
};
struct dsg_osc_2phase
{
    float phase[2];
};

static struct dsg_osc_phase* mk_phase()
{
    struct dsg_osc_phase* ret = (struct dsg_osc_phase*)calloc(1, sizeof(struct dsg_osc_phase));
    ret->phase = 0.0f;

    return ret;
}

dsg_osc_state dsg_osc_mk_pulse   (void) { return mk_phase(); }
dsg_osc_state dsg_osc_mk_sawtooth(void) { return mk_phase(); }
dsg_osc_state dsg_osc_mk_triangle(void) { return mk_phase(); }
dsg_osc_state dsg_osc_mk_noise   (void)
{
    struct dsg_osc_noise* ret = (struct dsg_osc_noise*)calloc(1, sizeof(struct dsg_osc_noise));

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    size_t ns = (size_t)ts.tv_nsec;

    ret->x = (((ns * ns) & 0x00FFFF00) >> 8) | ((ns & 0x000000FF) << 16);

    return ret;
}
dsg_osc_state dsg_osc_mk_sine   (void) { return mk_phase(); }
dsg_osc_state dsg_osc_mk_fm     (void)
{
    struct dsg_osc_2phase* ret = (struct dsg_osc_2phase*)calloc(1, sizeof(struct dsg_osc_2phase));

    return ret;
}

void dsg_osc_free_state(dsg_osc_state state)
{
    if (state)
        free(state);
}

struct dsg_osc dsg_osc_from_type(enum dsg_chan_type type)
{
    struct dsg_osc fail = (struct dsg_osc){
        .type     = type,
        .callback = NULL,
        .state    = NULL
    };

    switch (type)
    {
        case dsg_chan_soft_pulse:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_pulse,
                .state    = dsg_osc_mk_pulse()
            };
        case dsg_chan_soft_sawtooth:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_sawtooth,
                .state    = dsg_osc_mk_sawtooth()
            };
        case dsg_chan_soft_triangle:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_triangle,
                .state    = dsg_osc_mk_triangle()
            };
        case dsg_chan_soft_noise:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_noise,
                .state    = dsg_osc_mk_noise()
            };
        case dsg_chan_soft_sine:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_sine,
                .state    = dsg_osc_mk_sine()
            };
        case dsg_chan_soft_fm:
            return (struct dsg_osc){
                .type     = type,
                .callback = dsg_osc_fm,
                .state    = dsg_osc_mk_fm()
            };
        default:
            return fail;
    }
}

#define SAMPLE_RATE_RCP (1.0f / SAMPLE_RATE)

static inline void update_phase(float f, struct dsg_osc_phase* p)
{
    p->phase += f * SAMPLE_RATE_RCP;
    if (p->phase >= 1.0f) // will never be much larger than 1, because
        p->phase -= 1.0f; // frequencies much higher than the nyquist
                          // freq. are filtered out in render_buf_soft
}
static inline void update_2phase(float f, float h, struct dsg_osc_2phase* p)
{
    p->phase[0] += f * SAMPLE_RATE_RCP;
    if (p->phase[0] >= 1.0f)
        p->phase[0] -= 1.0f;

    p->phase[1] += h * SAMPLE_RATE_RCP;
    if (p->phase[1] >= 1.0f)
        p->phase[1] -= 1.0f;
}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
float dsg_osc_pulse(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    struct dsg_osc_phase* ps = (struct dsg_osc_phase*)state;

    float ret = (ps->phase < chan->duty_cycle) ? -1.0f : 1.0f;

    update_phase(freq, ps);

    return ret;
}
float dsg_osc_sawtooth(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    // this one borks with arpeggios and sudden freq changes
    // but I don't want to just delete the code...
    /*float p = 1.0f / freq;
    uint64_t max = (uint64_t)(0x4000 * p);

    // find the smallest (2^n)-1 where (1 << n) > max
    max--;
    max |= max >> 0x01;
    max |= max >> 0x02;
    max |= max >> 0x04;
    max |= max >> 0x08;
    max |= max >> 0x10;
    max |= max >> 0x20;

    // reduce the maximum value of t to avoid precision loss in the next
    // multiplication. this is probably a good way to enhance audio
    // quality while still preserving speed
    //
    // who am I kidding, this is 8 bit 8 kHz PCM
    // (but I'm still doing it)
    sample &= max;

    return (uint8_t)((uint64_t)((sample << 0xD) * p) & 0xFF);*/

    struct dsg_osc_phase* ps = (struct dsg_osc_phase*)state;

    float ret = ps->phase * 2.0f - 1.0f;

    update_phase(freq, ps);

    return ret;
}
float dsg_osc_triangle(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    struct dsg_osc_phase* ps = (struct dsg_osc_phase*)state;

    // TODO: optimize
    float y;
    if (ps->phase < chan->duty_cycle)
        y = ps->phase * (2.0f / chan->duty_cycle) - 1.0f;
    else
        y = (chan->duty_cycle - ps->phase) * (2.0f / (1.0f - chan->duty_cycle)) + 1.0f;

    update_phase(freq, ps);

    return y;
}
float dsg_osc_noise(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    struct dsg_osc_noise* ns = (struct dsg_osc_noise*)state;

    float ret = (ns->x & 0xFF) * (2.0f / 0xFF) - 1.0f;

    // good enough
    ns->x = (ns->x * 65 + 337) >> 4;

    return ret;
}
float dsg_osc_sine(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    struct dsg_osc_phase* ps = (struct dsg_osc_phase*)state;

    // TODO: optimize
    float x, neg = 1.0f;
    if (ps->phase <= 0.25f)
        x = ps->phase * TAU_F;
    else if (ps->phase >= 0.75f)
        x = (ps->phase - 1.0f) * TAU_F;
    else if (ps->phase <= 0.5f)
    {
        x = (ps->phase - 0.5f) * TAU_F;
        neg = -1.0f;
    }
    else
    {
        x = (ps->phase - 0.5f) * TAU_F;
        neg = -1.0f;
    }

    float ret = fast_sin(x) * neg;

    update_phase(freq, ps);

    return ret;
}
float dsg_osc_fm(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan)
{
    struct dsg_osc_2phase* ps = (struct dsg_osc_2phase*)state;

    // TODO: optimize
    float x, neg = 1.0f;
    if (ps->phase[0] <= 0.25f)
        x = ps->phase[0] * TAU_F;
    else if (ps->phase[0] >= 0.75f)
        x = (ps->phase[0] - 1.0f) * TAU_F;
    else if (ps->phase[0] <= 0.5f)
    {
        x = (ps->phase[0] - 0.5f) * TAU_F;
        neg = -1.0f;
    }
    else
    {
        x = (ps->phase[0] - 0.5f) * TAU_F;
        neg = -1.0f;
    }

    float ret = fast_cos(x) * neg;

    neg = 1.0f;
    if (ps->phase[1] <= 0.25f)
        x = ps->phase[1] * TAU_F;
    else if (ps->phase[1] >= 0.75f)
        x = (ps->phase[1] - 1.0f) * TAU_F;
    else if (ps->phase[1] <= 0.5f)
    {
        x = (ps->phase[1] - 0.5f) * TAU_F;
        neg = -1.0f;
    }
    else
    {
        x = (ps->phase[1] - 0.5f) * TAU_F;
        neg = -1.0f;
    }

    ret *= fast_cos(x) * neg * chan->h_amplitude;

    update_2phase(freq, chan->harmony, ps);

    return ret;
}
#pragma clang diagnostic pop

