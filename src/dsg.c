
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#include <portaudio.h>

#include "frontend.h"
#include "dsg.h"

struct be_data
{
    struct dsg_frontend frontend;
    void* fe_data;
};
struct cb_data
{
    size_t id;
    struct dsg_config* conf;
#ifndef DSG_DISABLE_FLOPPY
    dsg_rpi_gpio gpio;
    volatile struct dsg_floppy_ctl* floppy_ctl;
    dsg_floppy_bg_thread floppy_bg;
#endif
    volatile struct dsg_buf_queue* queue;
    PaStream* stream;

    size_t restb_skip;
    float* restb;
    size_t restb_size;
    bool restb_is_copy;
};

//static FILE* thef = NULL;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
static PaStreamCallbackResult portaudio_cb(const void* input,
        void* output, unsigned long frames,
        const PaStreamCallbackTimeInfo* timeInfo,
        PaStreamCallbackFlags flags, void* userdata)
#pragma clang diagnostic pop
{
    float* op = (float*)output;

    struct cb_data ud = *(struct cb_data*)userdata;

    if (ud.restb && ud.restb_size)
    {
        if (ud.restb_size - ud.restb_skip > frames)
        {
            memcpy(op, ud.restb + ud.restb_skip, frames * sizeof(float));
            ud.restb_skip += frames;

            return paContinue;
        }
        else
        {
            size_t d = ud.restb_size - ud.restb_skip;
            memcpy(op, ud.restb + ud.restb_skip, d * sizeof(float));
            frames -= d;
            op     += d;

            if (ud.restb_is_copy)
                free(ud.restb);

            ud.restb         = NULL ;
            ud.restb_size    = 0    ;
            ud.restb_skip    = 0    ;
            ud.restb_is_copy = false;
        }
    }

    while (frames)
    {
        struct cb_data* udp = (struct cb_data*)userdata;

        struct dsg_buf b = dsg_buf_dequeue(&udp->queue);

        if (!b.buf || !b.size)
        {
            printf("W: %zu: buffer underrun! (0x%lX frames)\n", ud.id, frames);

            memset(op, 0x00, sizeof(float) * frames);

            return paContinue;
        }

        if (frames < b.size)
        {
            memcpy(op, b.buf, frames * sizeof(float));

            ud.restb_skip    = frames   ;
            ud.restb_size    = b.size   ;
            ud.restb_is_copy = b.is_copy;
            ud.restb         = b.buf    ;

            return paContinue;
        }
        else
        {
            memcpy(op, b.buf, b.size * sizeof(float));
            frames -= b.size;
            op     += b.size;

            if (b.is_copy)
                free(b.buf);
        }
    }

    return paContinue;
}

static void bhandler_cb(size_t buf_len, void* buffer, void* userdata)
{
    struct cb_data* ud = (struct cb_data*)userdata;

    if (ud->conf->chan_cfg[ud->id].type == dsg_chan_none)
        return;

    if (ud->conf->chan_cfg[ud->id].type == dsg_chan_hard_floppy)
#ifndef DSG_DISABLE_FLOPPY
        dsg_floppy_enqueue(ud->floppy_bg, buf_len, (float*)buffer, false);
#else
    ;
#endif
    else
    {
        /*if (ud->id == 2)
        {
            fwrite(buffer, buf_len, sizeof(float), thef);
            fflush(thef);
        }*/

        dsg_buf_enqueue(&ud->queue, buf_len, buffer, false);
    }
}

void* dsg_init(struct dsg_frontend frontend)
{
    if (!frontend.init_fn || !frontend.mk_player_fn || !frontend.run_fn
            || !frontend.kill_fn)
    {
        printf("E: frontend struct invalid.\n");

        return NULL;
    }

    PaError err = Pa_Initialize();

    if (err != paNoError)
    {
        printf("E: could not initialise PortAudio: %i %s\n",
                err, Pa_GetErrorText(err));
        return NULL;
    }

    void* fe_data = frontend.init_fn(frontend);
    if (!fe_data)
    {
        printf("E: could not initialise the frontend.\n");

        err = Pa_Terminate();

        if (err != paNoError)
            printf("E: could not terminate PortAudio: %i %s\n", err, Pa_GetErrorText(err));
    }

    struct be_data* be_data = (struct be_data*)calloc(1, sizeof(struct be_data));

    be_data->frontend = frontend;
    be_data->fe_data  = fe_data ;

    return be_data;
}

bool dsg_run(struct dsg_config* conf, void* dsg_data)
{
    if (!conf || !dsg_data)
        return true;

    //thef = fopen("audio.bin", "a+");

    struct be_data be_data = *(struct be_data*)dsg_data;

    PaError err = 0;

    struct dsg_player* player =
        be_data.frontend.mk_player_fn(conf, be_data.fe_data);
    if (!player)
    {
        printf("E: dsg_player_new returned null.\n");
        return true;
    }

#ifndef DSG_DISABLE_FLOPPY
    dsg_rpi_gpio gpio = dsg_rpi_init(dsg_gpio_mmap_mem);
    if (!gpio)
    {
        printf("E: dsg_rpi_init returned null.\n");
        return true;
    }
#endif

    bool retv = false;

#ifndef DSG_DISABLE_FLOPPY
    volatile struct dsg_floppy_ctl* floppy_ctls [conf->chan_amt];
    memset(floppy_ctls , 0, sizeof(struct dsg_floppy_ctl*) * conf->chan_amt);
    dsg_floppy_bg_thread            floppy_bgs  [conf->chan_amt];
    memset(floppy_bgs  , 0, sizeof(dsg_floppy_bg_thread  ) * conf->chan_amt);
#endif
    PaStream*                       streams     [conf->chan_amt];
    memset(streams     , 0, sizeof(PaStream             *) * conf->chan_amt);
    struct cb_data                  bufh_udata  [conf->chan_amt];
    memset(bufh_udata  , 0, sizeof(struct cb_data        ) * conf->chan_amt);
    struct dsg_handle_buffer_cb     buf_handlers[conf->chan_amt];
    memset(buf_handlers, 0, sizeof(struct dsg_handle_buffer_cb) * conf->chan_amt);

    for (size_t i = 0; i < conf->chan_amt; ++i)
    {
        if (conf->chan_cfg[i].type == dsg_chan_none)
            continue;

#ifndef DSG_DISABLE_FLOPPY
        {
            struct dsg_cfg_chan ccfg = conf->chan_cfg[i];

            if (ccfg.type == dsg_chan_hard_floppy)
            {
                if (!(floppy_ctls[i] = dsg_floppy_make(ccfg.chan_opts.hard_floppy)))
                    printf("E: dsg_floppy_make (%zu) returned null.\n", i);
                else if (!(floppy_bgs [i] = dsg_floppy_spawn_bg_thread(floppy_ctls[i], gpio)))
                    printf("E: dsg_floppy_spawn_bg_thread (%zu) returned null\n", i);
            }
            else
            {
                floppy_ctls[i] = NULL;
                floppy_bgs [i] = NULL;
            }
        }
#endif
        {
            if (conf->chan_cfg[i].type == dsg_chan_hard_floppy)
                streams[i] = NULL;
            else
            {
                if ((err = Pa_OpenDefaultStream(&streams[i], 0, 1, paFloat32, SAMPLE_RATE,
                    dsg_player_get_buf_size(player)/*paFramesPerBufferUnspecified*/,
                    (PaStreamCallback*)portaudio_cb, &bufh_udata[i])) != paNoError)
                {
                    printf("E: Couldn't open stream %zu: %i %s\n",
                            i, err, Pa_GetErrorText(err));

                    retv = true;
                    goto FREE_ALL;
                }
            }

            bufh_udata[i] = (struct cb_data){
                .id   = i   ,
                .conf = conf,
#ifndef DSG_DISABLE_FLOPPY
                .gpio = gpio,

                .floppy_ctl = floppy_ctls[i],
                .floppy_bg  = floppy_bgs [i],
#endif
                .queue  = NULL      ,
                .stream = streams[i],

                .restb_skip = 0   ,
                .restb      = NULL,
                .restb_size = 0   ,
                .restb_is_copy = false
            };
            buf_handlers[i] = (struct dsg_handle_buffer_cb){
                .userdata = &bufh_udata[i],
                .callback = bhandler_cb
            };
        }

        if (streams[i])
            if ((err = Pa_StartStream(streams[i])) != paNoError)
            {
                printf("E: couldn't start stream %zu: %i %s\n",
                        i, err, Pa_GetErrorText(err));

                retv = true;
                goto FREE_ALL;
            }
    }

    bool res = be_data.frontend.run_fn(conf, be_data.fe_data, player
#ifndef DSG_DISABLE_FLOPPY
        , gpio, floppy_ctls, floppy_bgs
#endif
        , buf_handlers);
    if (res)
        printf("E: frontend runner exited with an error.\n");

FREE_ALL:
    for (size_t i = 0; i < conf->chan_amt; ++i)
    {
        if (conf->chan_cfg[i].type == dsg_chan_none)
            continue;

        if (streams[i])
        {
            if ((err = Pa_StopStream(streams[i])) != paNoError)
                printf("E: couldn't stop stream %zu: %i %s\n",
                        i, err, Pa_GetErrorText(err));
            if ((err = Pa_CloseStream(streams[i])) != paNoError)
                printf("E: couldn't close stream %zu: %i %s\n",
                        i, err, Pa_GetErrorText(err));
        }

#ifndef DSG_DISABLE_FLOPPY
        if (floppy_bgs[i])
            dsg_floppy_kill_bg_thread(floppy_bgs[i]);
        if (floppy_ctls[i])
            dsg_floppy_free(floppy_ctls[i]);
#endif

        struct cb_data ud = *(struct cb_data*)&bufh_udata[i];

        if (ud.restb && ud.restb_is_copy)
            free(ud.restb);

        dsg_buf_free(ud.queue, true);
    }

    dsg_player_free(player);
#ifndef DSG_DISABLE_FLOPPY
    dsg_rpi_kill(gpio);
#endif

    //fclose(thef);

    return retv;
}

void dsg_kill(void* dsg_data)
{
    struct be_data be_data = *(struct be_data*)dsg_data;

    be_data.frontend.kill_fn(be_data.frontend, be_data.fe_data);

    PaError err = Pa_Terminate();

    if (err != paNoError)
        printf("E: could not terminate PortAudio: %i %s\n", err, Pa_GetErrorText(err));
}

