
#include <stdlib.h>
#include <string.h>

#include "midi.h"

/*struct dsg_midi dsg_midi_parse(const struct dsg_midi_raw* raw)
{
    static struct dsg_midi fail = (struct dsg_midi)
    {
        .type = dsg_midi_unknown_message,
        .raw  = (struct dsg_midi_raw)
        {
            .status = 0x00,
            .data   = { 0xFA, 0x17 }
        }
    };

    if (!raw)
        return fail;

    uint8_t status = raw->status;
    if ((status & 0xF0) == 0xF0) // system
    {
        struct dsg_midi ret = (struct dsg_midi)
        {
            .type = dsg_midi_system_message,
            .sys  = (struct dsg_midi_sysmsg)
            {
                .msg_type   = status,
                .sysex_len  = 0,
                .sysex_data = NULL
            }
        };

        switch (status)
        {
            case dsg_midi_timingcd :
            case dsg_midi_songpos  :
            case dsg_midi_songsel  :
            case dsg_midi_tunereq  :
            case dsg_midi_timingclk:
            case dsg_midi_startseq :
            case dsg_midi_contseq  :
            case dsg_midi_stopseq  :
            case dsg_midi_activesns:
            case dsg_midi_sysreset :
                ret.sys.data[0] = raw->data[0];
                ret.sys.data[1] = raw->data[1];
                break;
            case dsg_midi_sysex_start:
                {
                    size_t   len = 0;
                    const uint8_t *dat = &raw->data[0],
                                  *dat_orig = dat;

                    while (*dat != dsg_midi_sysex_end)
                    {
                        len++;
                        dat++;
                    }

                    ret.sys.sysex_len  = len;
                    ret.sys.sysex_data = (uint8_t*)malloc(len);

                    memcpy(ret.sys.sysex_data, dat_orig, len);
                }
                break;
            default:
                ret.type = dsg_midi_unknown_message;
                ret.raw  = *raw;
                break;
        }

        return ret;
    }
    // channel
    struct dsg_midi ret = (struct dsg_midi)
    {
        .type = dsg_midi_channel_message,
        .chan = (struct dsg_midi_chanmsg)
        {
            .msg_type = status & 0xF0,
            .channel  = status & 0x0F,

            .note     = 0,
            .velocity = 0
        }
    };

    switch (status & 0xF0)
    {
        case dsg_midi_note_off:
        case dsg_midi_note_on :
        case dsg_midi_pkeypres:
        case dsg_midi_cchange :
        case dsg_midi_pchange :
        case dsg_midi_chanpres:
        case dsg_midi_pitchbnd:
            ret.chan.note     = raw->data[0];
            ret.chan.velocity = raw->data[1];
            break;

        default:
            ret.type = dsg_midi_unknown_message;
            ret.raw  = *raw;
            break;
    }

    return ret;
}*/

size_t dsg_midi_packet_size(const uint8_t* raw)
{
    size_t i;

    if (raw[0] == dsg_midi_sysex_start)
    {
        for (i = 1; raw[i] != dsg_midi_sysex_end; ++i) ;

        return i + 1;
    }

    if (!(raw[0] & 0x80)) return 0;

    for (i = 1; !(raw[i] & 0x80) && i < 3; ++i) ;

    return i;
}

