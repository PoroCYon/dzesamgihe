
#include <math.h>
#include <string.h>

#include <stdio.h>

#include "config.h"
#include "render.h"
#include "oscs.h"

static void render_buf_soft(size_t samples_to_render, struct dsg_channel* chan, struct dsg_osc osc, float* destbuf)
{
    for (uint64_t s = 0; s < samples_to_render; ++s)
    {
        /*if (!chan->playing)
        {
            memset(destbuf, 0, sizeof(float) * 0x80);
            s += 0x7F;
            continue; // will do s++;
        }*/

        uint64_t t = chan->time_total + s;

        // ?flip consequent naming
        chan->volume      += chan->volume_del  * SAMPLE_RATE_RCP;
        chan->extra_pitch += chan->pitch_slide * SAMPLE_RATE_RCP;
        chan->duty_cycle  += chan->duty_sweep  * SAMPLE_RATE_RCP;

        if (chan->duty_cycle >= 0.9f) // clip from 0.1 to 0.9, the extremes
            chan->duty_cycle -= 0.8f; // aren't much fun to listen to
        if (chan->volume < 0.0f)      // this is possible, because duty cycles
            chan->volume = 0.0f;      // x and 1 - x have the same timbre
        if (chan->volume > 1.0f)
            chan->volume = 1.0f;

        union dsg_arp_offsets arp_offs = dsg_read_arp(chan->arpeggio);
        float as = (float)chan->arp_speed;
        if (as > 0.0f)
            as = 1.0f / -as;
        else
            as = -as;
        int8_t aoff = arp_offs.offsets[(size_t)((float)(t >> 7) * as) & 3];

        float final_pitch = chan->extra_pitch + aoff + chan->base_note;
        float final_freq  = dsg_note_to_freq(final_pitch);

#ifndef DSG_DISABLE_FLOPPY
        // note: cheating happens here
        if (osc.type == dsg_chan_hard_floppy)
            destbuf[s] = final_freq;
        else
#endif
        if (final_freq > (SAMPLE_RATE >> 1)) // above Nyquist freq.
            destbuf[s] = 0.0f;
        else if (osc.callback)
            destbuf[s] = osc.callback(t, final_freq, osc.state, chan) * chan->volume * chan->volume_temp;
    }

    chan->time_total += samples_to_render;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"

static void render_f32_to_u8(size_t buf_len, uint8_t* dest, float* src)
{
    for (size_t i = 0; i < buf_len; ++i)
        dest[i] = (uint8_t)(src[i] * 127.5f + 127.5f); // add *after* multiplying
                                                       // to minimise rounding
                                                       // errors
}

#pragma clang diagnostic pop

void dsg_render_player(struct dsg_player* player, struct dsg_handle_buffer_cb* buf_handlers)
{
    if (!player || !buf_handlers)
        return;
    if (!player->tempo)
        return;

    for (size_t i = 0; i < player->chan_amt; ++i)
        dsg_render_chan(player, i, buf_handlers[i]);
}
void dsg_render_chan(struct dsg_player* player, size_t c, struct dsg_handle_buffer_cb buf_handler)
{
    if (!player || !buf_handler.callback)
        return;
    if (c >= player->chan_amt || !player->tempo)
        return;

    struct dsg_channel* chan = &player->channels[c];

#ifdef DSG_DISABLE_FLOPPY
    if (chan->type == dsg_chan_hard_floppy)
        return;
#endif

    size_t buf_len = dsg_player_get_buf_size(player);

    float rbuf[buf_len];

    if (chan->muted || !chan->playing || chan->type == dsg_chan_none)
    {
        memset(rbuf, 0x00, sizeof(float) * buf_len);
        buf_handler.callback(buf_len, rbuf, buf_handler.userdata);
    }
#ifndef DSG_DISABLE_FLOPPY
    else if (chan->type == dsg_chan_hard_floppy)
    {
        render_buf_soft(buf_len, chan, chan->oscillator, rbuf);

        buf_handler.callback(buf_len, rbuf, buf_handler.userdata);
    }
#endif
    else
    {
        render_buf_soft(buf_len, chan, chan->oscillator, rbuf);

        // TODO: apply biquad
        // TODO: do it per sample, so freq cutoff slides etc. are possible?
        /*struct dsg_biquad_params biquad = SOMEHOW_GET_A_BIQUAD;
        if (biquad.mk_filter_cb)
        {
            struct dsg_biquad_coeffs_norm ncoeffs =
                dsg_biquad_norm_coeffs(
                    biquad.mk_filter_cb(biquad.centre_freq, biquad.gain, biquad.param)
                    );

            dsg_biquad_apply_buffer(buf_len, rbuf, &ncoeffs, &chan->SOMEHOW_GET_BIQUAD_STATE);
        }*/

        //uint8_t rrbuf[buf_len];

        //render_f32_to_u8(buf_len, rrbuf, rbuf);

        buf_handler.callback(buf_len, rbuf, buf_handler.userdata);
    }
}

