
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "config.h"
#include "gpio.h"
#include "floppy.h"

#include "net_lc.h"

int dsg_net_lc_main(struct dsg_chan_hard_floppy_opts floppy_opts, /*const char* addr,*/ uint16_t port)
{
    int retv = 0;

    dsg_rpi_gpio gpio;
    volatile struct dsg_floppy_ctl* floppy_ctl;
    dsg_floppy_bg_thread floppy_bg;

    if (!(gpio = dsg_rpi_init(dsg_gpio_mmap_mem)))
    {
        printf("rpi init rip\n");
        retv = 1;
        goto E_E;
    }
    if (!(floppy_ctl = dsg_floppy_make(floppy_opts)))
    {
        printf("floppy make rip\n");
        retv = 1;
        goto E_gpio;
    }
    if (!(floppy_bg = dsg_floppy_spawn_bg_thread(floppy_ctl, gpio)))
    {
        printf("spawn bg rip\n");
        retv = 1;
        goto E_floppy;
    }

    int sock;
    if ((sock = socket(PF_INET6, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        printf("socket rip\n");
        retv = errno;
        goto E_thread;
    }

    struct sockaddr_in6 sa;
    struct sockaddr si_other;
    memset(&sa, 0, sizeof(struct sockaddr_in6));
    sa.sin6_family      = AF_INET6;
    //inet_pton(AF_INET6, addr, &(sa.sin6_addr));
    sa.sin6_addr = in6addr_any;
    sa.sin6_port        = htons(port);

    if ((retv = bind(sock, (struct sockaddr*)&sa, sizeof(struct sockaddr_in6))) < 0)
    {
        printf("bind rip\n");
        goto E_socket;
    }

    const size_t buflen = 5512;
    float buf[buflen];

    socklen_t slen;
    for (;;) {
        ssize_t l;
        if ((l = recvfrom(sock, buf, buflen * sizeof(float), 0, &si_other, &slen)) < 0)
        {
            printf("recvfrom rip\n");
            break;
        }

        l >>= 2; // /= sizeof(float)
        dsg_floppy_enqueue(floppy_bg, (size_t)l, buf, false);
    }

E_socket:
    close(sock);
E_thread:
    dsg_floppy_kill_bg_thread(floppy_bg);
E_floppy:
    dsg_floppy_free(floppy_ctl);
E_gpio:
    dsg_rpi_kill(gpio);

E_E:
    return retv;
}

