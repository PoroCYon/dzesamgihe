#!/usr/bin/env bash

FORCE=0

for arg; do
    case $arg in
        -f)
            FORCE=1
            ;;
        *)
            echo "What is '$arg' supposed to be?"
            exit
            ;;
    esac
done

if [[ ! (-f "track.bin") ]] || [[ "$FORCE" == "1" ]]; then
    cat /dev/zero | head -c 640 > track.bin
    echo "An empty track is generated, but you'll have to create the config.bin file, which is also needed, yourself."
fi

