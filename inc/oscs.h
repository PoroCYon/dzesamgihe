
#ifndef OSCS_H_
#define OSCS_H_

#include <stddef.h>
#include <stdint.h>

#include "osc.h"
#include "chan.h"

dsg_osc_state dsg_osc_mk_pulse   (void);
dsg_osc_state dsg_osc_mk_sawtooth(void);
dsg_osc_state dsg_osc_mk_triangle(void);
dsg_osc_state dsg_osc_mk_noise   (void);
dsg_osc_state dsg_osc_mk_sine    (void);
dsg_osc_state dsg_osc_mk_fm      (void);

void dsg_osc_free_state(dsg_osc_state state);

float dsg_osc_pulse   (uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);
float dsg_osc_sawtooth(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);
float dsg_osc_triangle(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);
float dsg_osc_noise   (uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);
float dsg_osc_sine    (uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);
float dsg_osc_fm      (uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);

struct dsg_osc dsg_osc_from_type(enum dsg_chan_type type);

#endif

