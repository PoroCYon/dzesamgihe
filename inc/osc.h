
#ifndef DSG_OSC_H_
#define DSG_OSC_H_

#include <stddef.h>
#include <stdint.h>

#include "config.h"

typedef void* dsg_osc_state;

struct dsg_channel;
typedef float (*dsg_osc_fn)(uint64_t sample, float freq, dsg_osc_state state, struct dsg_channel* chan);

// FM:
// a * sin(freq * t) * a_ * sin(harm * t)

struct dsg_osc
{
    enum dsg_chan_type type;
    dsg_osc_fn    callback;
    dsg_osc_state state   ;
};

#endif

