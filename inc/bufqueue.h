
#ifndef DSG_BUF_QUEUE
#define DSG_BUF_QUEUE

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

struct dsg_buf
{
    float* buf ;
    size_t size;
    bool is_copy;
};

struct dsg_buf_queue
{
    volatile struct dsg_buf_queue* next;
    struct dsg_buf buf;
};

void dsg_buf_enqueue(volatile struct dsg_buf_queue*volatile* q,
        size_t buf_size, float* buf, bool no_copy);

struct dsg_buf dsg_buf_dequeue(volatile struct dsg_buf_queue*volatile* q);

void dsg_buf_free(volatile struct dsg_buf_queue* q, bool recurse);

void dsg_buf_print(volatile struct dsg_buf_queue* q, size_t cookie);

#endif

