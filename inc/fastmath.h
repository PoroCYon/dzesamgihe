
#ifndef FAST_MATH_H_
#define FAST_MATH_H_

#include <stddef.h>
#include <stdint.h>

#define HALF_PI_D (1.5707963267948966192313216916398 )
#define HALF_PI_F (1.5707963267948966192313216916398f)

#define PI_D (3.1415926535897932384626433832795 )
#define PI_F (3.1415926535897932384626433832795f)

#define TAU_D (6.28318530717958647692528676655901 )
#define TAU_F (6.28318530717958647692528676655901f)

// this doesn't exist? wat?
size_t min(size_t a, size_t b);
size_t max(size_t a, size_t b);

int fast_sign_i(int    x);
int fast_sign_f(float  x);
int fast_sign_d(double x);

size_t gcd(size_t a, size_t b);
size_t lcm(size_t a, size_t b);

float  wrap_angle  (float  x);
double wrap_angle_d(double x);

float  fast_pow  (float  x, int64_t exp);
double fast_pow_d(double x, int64_t exp);

float fast_inv_sqrt(float x);

int fast_log2_u8 (uint8_t  b);
int fast_log2_u16(uint16_t s);
int fast_log2_u32(uint32_t u);
int fast_log2_u64(uint64_t l);

int fast_log2_i8 (int8_t  c);
int fast_log2_i16(int16_t s);
int fast_log2_i32(int32_t i);
int fast_log2_i64(int64_t l);

//NOTE: the following methods do NOT have range checking!

// accurate enough to be used in [-pi; pi], but not bad for other x
float  fast_atan  (float  x);
double fast_atan_d(double x);

float  fast_atan2  (float  y, float  x);
double fast_atan2_d(double y, double x);

float  fast_atan_inv  (float  x);
double fast_atan_inv_d(double x);

// only in [-pi/2; pi/2], but error margin is only -80dB (within the range)
float  fast_sin  (float  x);
double fast_sin_d(double x);

float  fast_cos  (float  x);
double fast_cos_d(double x);

#endif
