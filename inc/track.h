
#ifndef DSG_TRACK_H_
#define DSG_TRACK_H_

#include <stddef.h>
#include <stdint.h>

// me nn fv vv

enum dsg_meta_op
{
    dsg_meta_nothing = 0x00,
    dsg_meta_stop    = 0x10, // global
    dsg_meta_ignore  = 0x20, // for 'comments' (0x20 was deliberately chosen)
    dsg_meta_mute    = 0x30, // channel
    dsg_meta_unmute  = 0x40, // channel
};

enum dsg_event_type
{
    dsg_event_nochange = 0x00,
    dsg_event_note_on  = 0x01,
    dsg_event_note_off = 0x02,
    dsg_event_control  = 0x03,
};

// semitones, 0x31 = 440 Hz = A4
enum dsg_note_value
{
    dsg_note_C_m1 = 0x00,
    dsg_note_A_4  = 0x45,
    // TODO: fill with MIDI notes
    // TODO: not really needed?
};
enum dsg_control_value
{
    dsg_ctl_none = 0x00,
    // TODO: define possible control change commands
    // TODO: then implement them in chan.c
};

enum dsg_effect_type
{
    dsg_effect_none        = 0x00,
    dsg_effect_duty_cycle  = 0x10, // chan | 0x000-0xFFF | sets the duty cycle (def. 0x800)
    dsg_effect_fm_harmony  = 0x10, // chan | 0x000-0xFFF | sets the FM harmony
    // TODO: harmony sweep?
    dsg_effect_duty_sweep  = 0x20, // chan | 0x000-0xFFF | signed duty cycle delta (sweep/0x80 every sample)
    dsg_effect_fm_hamp     = 0x30, // chan | 0x000-0xFFF | sets the FM harmony amplitude
    dsg_effect_arpeggio    = 0x30, // chan | 0xABC       | note; note + A; note + B; note + C (A, B, C signed)
    // NOTE: 1 byte free
    dsg_effect_arps_td_biq = 0x40, // chan | 0xABC       | sets arp speed to A if A!=0xF, biased to 7 (arps is exponential)
                                   //                    | sets biquad type (none, lowpass, highpass, bandpassQpeak, bandpass, notch, allpass, peakingEQ, lowshelf, highshelf)
                                   //                    | sets time div (unsigned) if C!=0
    // TODO: set extra ptich
    dsg_effect_pitch_slide = 0x50, // chan | 0x000-0xFFF | pitch delta (signed, semitones/8), pitch += delta/0x80 samples
    dsg_effect_tempo       = 0x60, // glob | 0x000-0xFFF | sets tempo (bpm) (unsigned)
    dsg_effect_tempo_delta = 0x70, // glob | 0x000-0xFFF | adds tempo delta (signed), tempo += delta/0x80 samples
    // TODO: make ctl?
    dsg_effect_jump_rel    = 0x80, // glob | 0x000-0xFFF | relative jump (signed)
    dsg_effect_jump_abs    = 0x90, // glob | 0x000-0xFFF | absolute jump (unsigned)
    dsg_effect_volume      = 0xA0, // chan | 0x000-0xFFF | set volume (unsigned)
    dsg_effect_volume_temp = 0xB0, // chan | 0x000-0xFFF | as above, but resets every noteon/off
    dsg_effect_volume_del  = 0xC0, // chan | 0x000-0xFFF | delta volume (signed), vol += delta/0x80
    dsg_effect_extra_pitch = 0xD0, // chan | 0x000-0xFFF | signed, note += extra_pitch / 8

    // NOTE: reserved, but not in use
    // TODO: move these to CChange
    /*dsg_effect_biquad_freq = 0xD0,
    dsg_effect_biquad_qrcp = 0xE0,
    dsg_effect_biquad_gain = 0xF0*/
};

#pragma pack(push,1)
struct dsg_track_entry
{
    // this should be endianness-agnostic
    uint8_t meta_event;
    uint8_t note_ctl;
    uint8_t effect_1;
    uint8_t effect_2;
};
#pragma pack(pop)

static inline enum dsg_meta_op dsg_get_meta(struct dsg_track_entry entry)
{
    return (enum dsg_meta_op)(entry.meta_event & 0xF0);
}
static inline enum dsg_event_type dsg_get_event(struct dsg_track_entry entry)
{
    return (enum dsg_event_type)(entry.meta_event & 0x0F);
}
static inline enum dsg_note_value dsg_get_note(struct dsg_track_entry entry)
{
    return (enum dsg_note_value)entry.note_ctl;
}
static inline enum dsg_control_value dsg_get_control(struct dsg_track_entry entry)
{
    return (enum dsg_control_value)entry.note_ctl;
}
static inline enum dsg_effect_type dsg_get_effect_type(struct dsg_track_entry entry)
{
    return (enum dsg_effect_type)(entry.effect_1 & 0xF0);
}
static inline uint16_t dsg_get_effect_data_u(struct dsg_track_entry entry)
{
    return (uint16_t)(((uint16_t)(entry.effect_1 & 0x0F) << 8) |  (uint16_t ) entry.effect_2);
}
static inline int16_t dsg_get_effect_data_s(struct dsg_track_entry entry)
{
    uint16_t uv = dsg_get_effect_data_u(entry);

    if (uv & 0x800)
        return -(int16_t)(uv & 0x7FF);
    else
        return  (int16_t)(uv & 0x7FF);
}

union dsg_arp_offsets
{
    int8_t offsets[4];
    struct
    {
        int8_t nbase;
        int8_t arp_a;
        int8_t arp_b;
        int8_t arp_c;
    };
};
static inline union dsg_arp_offsets dsg_read_arp(uint16_t arpval)
{
    union dsg_arp_offsets retval;

    retval.nbase = 0;
    retval.arp_a = (arpval & 0x700) >> 8;
    retval.arp_b = (arpval & 0x070) >> 4;
    retval.arp_c =  arpval & 0x007      ;

    if (arpval & 0x800)
        retval.arp_a = -retval.arp_a;
    if (arpval & 0x080)
        retval.arp_b = -retval.arp_b;
    if (arpval & 0x008)
        retval.arp_c = -retval.arp_c;

    return retval;
}

#endif

