
#ifndef DSG_PLAYER_H_
#define DSG_PLAYER_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#include "track.h"
#include "config.h"
#include "chan.h"

struct dsg_player
{
    size_t row_amt ;
    size_t chan_amt; // cols in hex file

    struct dsg_config     * config  ;
    struct dsg_track_entry* track   ;
    struct dsg_channel    * channels;

     size_t tempo    ; // bpm
     size_t time_div ;
     size_t cur_row  ;
    int16_t tempo_del;
    bool   playing  ;
};

static inline struct dsg_track_entry* dsg_player_entry_at(struct dsg_player* player, size_t row, size_t chan)
{
    return &player->track[row * player->chan_amt + chan];
}
static inline struct dsg_track_entry dsg_player_get_entry(struct dsg_player* player, size_t row, size_t chan)
{
    return  player->track[row * player->chan_amt + chan];
}

static inline void dsg_player_upd_chan(struct dsg_player* player, size_t chan, struct dsg_track_entry cur_entry)
{
    dsg_channel_upd(&player->channels[chan], cur_entry);
}

struct dsg_player* dsg_player_new(size_t rows, struct dsg_config* cfg, struct dsg_track_entry* entries);
struct dsg_player* dsg_player_new_notrack(struct dsg_config* cfg);

void dsg_player_free(struct dsg_player* player);

void dsg_player_play(struct dsg_player* player, bool force_chans);

void dsg_player_step(struct dsg_player* player);

static inline size_t dsg_player_get_buf_size(struct dsg_player* player)
{
    // NOTE: if you edit this, do edit config.h

    if (!player)
        return 0;

    if (!player->row_amt || !player->track || !player->tempo || !player->time_div)
        return DEFAULT_BUF_SIZE;

    size_t size = (60 * SAMPLE_RATE) / (player->tempo * player->time_div);

    if (size & 7)
        size = (size & ~(size_t)7) + 8;

    return size;
}

#endif

