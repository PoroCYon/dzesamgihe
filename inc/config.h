
#ifndef DSG_CONFIG_H_
#define DSG_CONFIG_H_

#include <stddef.h>
#include <stdint.h>

#include "track.h"

#define SAMPLE_RATE     (       0x4000     )
#define SAMPLE_RATE_RCP (1.0f / SAMPLE_RATE)

#define MSEC_IN_SEC (1000 *           1)
#define USEC_IN_SEC (1000 * MSEC_IN_SEC)
#define NSEC_IN_SEC (1000 * USEC_IN_SEC)

#define DEFAULT_BUF_SIZE (0x400)

enum dsg_chan_type
{
    dsg_chan_none          = 0x00,
    dsg_chan_hard_floppy   = 0x01,
    // TODO: fix
    dsg_chan_soft_pulse    = 0x02,
    // TODO: fix
    dsg_chan_soft_sawtooth = 0x03, // DC is clipped between 0.1 and 0.9,
    dsg_chan_soft_triangle = 0x04, // so a separate one for pure sawtooth
    dsg_chan_soft_noise    = 0x05, // waves is needed (sawtooths are always
    dsg_chan_soft_sine     = 0x06, // nice in combination with a lowpass filter)
    dsg_chan_soft_fm       = 0x07
};

#pragma pack(push,1)
struct dsg_chan_hard_floppy_opts
{
    uint8_t pin_dir_phys ;
    uint8_t pin_step_phys;
};
union dsg_chan_opts
{
    struct dsg_chan_hard_floppy_opts hard_floppy;
};
struct dsg_cfg_chan
{
    uint8_t /*enum dsg_chan_type*/ type;

    uint8_t mn_min;
    uint8_t mn_max;
    union dsg_chan_opts chan_opts;
};

struct dsg_config
{
    uint8_t chan_amt;
    uint8_t tempo   ; // bpm
    uint8_t time_div;
    // * (tempo*time div)/0x3C = track entries/second
    // * 1 second = SAMPLE_RATE bytes/s
    // => 1 buffer = (60 * SAMPLE_RATE)/(tempo*time div) bytes/beat

    struct dsg_cfg_chan chan_cfg[];
};

static inline size_t dsg_get_buffer_size(struct dsg_config* cfg)
{
    // NOTE: if you edit this, do edit player.h

    if (!cfg)
        return 0;

    if (!cfg->tempo || !cfg->time_div)
        return 0x100;

    size_t size = (60 * SAMPLE_RATE) / (cfg->tempo * cfg->time_div);

    // pad properly, so audio libs will accept the stream
    if (size & 7)
        size = (size & ~(size_t)7) + 8;

    return size;
}
static inline size_t dsg_get_rows(size_t track_file_size, struct dsg_config* cfg)
{
    return track_file_size / (cfg->chan_amt * sizeof(struct dsg_track_entry));
}

static inline size_t dsg_get_chan_from_note(size_t note, struct dsg_config* cfg)
{
    for (size_t i = 0; i < cfg->chan_amt; ++i)
        if (note >= cfg->chan_cfg[i].mn_min && note <= cfg->chan_cfg[i].mn_max)
            return i;

    return ~(size_t)0;
}

#endif

