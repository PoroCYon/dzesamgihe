
#ifndef DSG_DISABLE_FLOPPY
#ifndef DSG_GPIO_H_
#define DSG_GPIO_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

enum dsg_gpio_mode
{
    dsg_gpio_mmap_gpiomem = 0,
    dsg_gpio_mmap_mem     = 1,
    dsg_gpio_sysfs        = 2
};

enum dsg_gpio_pin_type
{
    dsg_gpio_pin_physical,
    dsg_gpio_pin_broadcom,
    dsg_gpio_pin_wiringpi
};
enum dsg_gpio_pin_dir
{
    dsg_gpio_pin_input,
    dsg_gpio_pin_output
};

enum dsg_rpi_model
{
    dsg_rpi_a     = 0,
    dsg_rpi_b     = 1,
    dsg_rpi_aplus = 2,
    dsg_rpi_bplus = 3,
    dsg_rpi_2     = 4,
    dsg_rpi_alpha = 5,
    dsg_rpi_cm    = 6,

    dsg_rpi_3     = 8,
    dsg_rpi_zero  = 9
};
enum dsg_rpi_revision
{
    dsg_rpi_rev_1   = 0,
    dsg_rpi_rev_1_1 = 1,
    dsg_rpi_rev_1_2 = 2,
    dsg_rpi_rev_2   = 3
};
struct dsg_rpi_board_id
{
    enum dsg_rpi_model    model   ;
    enum dsg_rpi_revision revision;
  //bool new_layout;
};

typedef void* dsg_rpi_gpio;

dsg_rpi_gpio dsg_rpi_init(enum dsg_gpio_mode preferred_mode);

void dsg_rpi_kill(dsg_rpi_gpio gpio);


struct dsg_rpi_board_id dsg_rpi_get_board_id(void);

size_t dsg_rpi_convert_pin(/*dsg_rpi_gpio gpio,*/ size_t pin, enum dsg_gpio_pin_type from, enum dsg_gpio_pin_type to);


void dsg_gpio_set_pin_dir(dsg_rpi_gpio gpio, size_t pin_bcm, enum dsg_gpio_pin_dir dir);

void dsg_gpio_set_pin(dsg_rpi_gpio gpio, size_t pin_bcm, bool val);
bool dsg_gpio_get_pin(dsg_rpi_gpio gpio, size_t pin_bcm          );

#endif
#endif

