
#ifndef DSG_FRONTEND_H_
#define DSG_FRONTEND_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"
#include "player.h"
#include "render.h"
#include "gpio.h"
#include "floppy.h"
#include "midi.h"

struct dsg_frontend;

typedef void             * (*dsg_frontend_init_fn     )(struct dsg_frontend fe);
typedef struct dsg_player* (*dsg_frontend_mk_player_fn)(
        struct dsg_config* conf, void* data);
typedef bool               (*dsg_frontend_run_fn      )(
        struct dsg_config* conf, void* data, struct dsg_player* player
#ifndef DSG_DISABLE_FLOPPY
        , dsg_rpi_gpio gpio
        , volatile struct dsg_floppy_ctl* ctls
        , dsg_floppy_bg_thread* threads
#endif
        , struct dsg_handle_buffer_cb* buf_handlers
        );
// this function must free the data passed through, and the init_data
typedef void               (*dsg_frontend_kill_fn     )(
        struct dsg_frontend fe, void* data);

struct dsg_frontend
{
    void* init_data;

    dsg_frontend_init_fn      init_fn     ;
    dsg_frontend_mk_player_fn mk_player_fn;
    dsg_frontend_run_fn       run_fn      ;
    dsg_frontend_kill_fn      kill_fn     ;
};

struct dsg_frontend dsg_make_track_frontend(
        struct dsg_track_entry* track, size_t rows);
struct dsg_frontend dsg_make_midi_frontend (
        size_t port_amt, struct dsg_midi_port* ports);

#endif

