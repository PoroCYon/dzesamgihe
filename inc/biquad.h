
#ifndef DSG_BIQUAD_H_
#define DSG_BIQUAD_H_

#include <stddef.h>
#include <stdint.h>

struct dsg_biquad_coeffs
{
    float a0, a1, a2,
          b0, b1, b2;
};
struct dsg_biquad_coeffs_norm
{
    float c0, c1, c2, c3, c4;
};

struct dsg_biquad_state
{
    float prevx[2],
          prevy[2];
};

typedef struct dsg_biquad_coeffs (*dsg_biquad_mk_filter_fn)(float centre_freq, float gain, float param);
struct dsg_biquad_params
{
    dsg_biquad_mk_filter_fn mk_filter_cb;
    float centre_freq,
          gain       ,
          param      ; // 1/Q, or 1/S for shelving filters
};

static inline struct dsg_biquad_coeffs_norm dsg_biquad_norm_coeffs(struct dsg_biquad_coeffs cs)
{
    return (struct dsg_biquad_coeffs_norm){
        .c0 = cs.b0 / cs.a0,
        .c1 = cs.b1 / cs.a0,
        .c2 = cs.b2 / cs.a0,
        .c3 = cs.a1 / cs.a0,
        .c4 = cs.a2 / cs.a0
    };
}

float dsg_biquad_apply_sample(float sample,
    struct dsg_biquad_coeffs_norm* coeffs, struct dsg_biquad_state* state);
void  dsg_biquad_apply_buffer(size_t buf_len, float* buf,
    struct dsg_biquad_coeffs_norm* coeffs, struct dsg_biquad_state* state);

// NOTE: 'gain' is unused
struct dsg_biquad_coeffs dsg_biquad_mk_lowpass (float centre_freq, float gain, float Qrcp);
struct dsg_biquad_coeffs dsg_biquad_mk_highpass(float centre_freq, float gain, float Qrcp);

struct dsg_biquad_coeffs dsg_biquad_mk_bandpass_Q_peak_gain(float centre_freq, float gain, float Qrcp);
struct dsg_biquad_coeffs dsg_biquad_mk_bandpass_const_gain (float centre_freq, float gain, float Qrcp);

struct dsg_biquad_coeffs dsg_biquad_mk_notch  (float centre_freq, float gain, float Qrcp);
struct dsg_biquad_coeffs dsg_biquad_mk_allpass(float centre_freq, float gain, float Qrcp);

// NOTE: the following do use 'gain'
struct dsg_biquad_coeffs dsg_biquad_mk_peaking_EQ(float centre_freq, float gain, float Qrcp);

struct dsg_biquad_coeffs dsg_biquad_mk_low_shelf (float centre_freq, float gain, float Srcp);
struct dsg_biquad_coeffs dsg_biquad_mk_high_shelf(float centre_freq, float gain, float Srcp);

#endif

