
#ifndef DSG_DSG_H_
#define DSG_DSG_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"
#include "render.h"
#include "floppy.h"
#include "bufqueue.h"
#include "frontend.h"

void* dsg_init(struct dsg_frontend frontend);

bool dsg_run(struct dsg_config* conf, void* dsg_data);

void dsg_kill(void* dsg_data);

#endif

