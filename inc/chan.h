
#ifndef DSG_CHAN_H_
#define DSG_CHAN_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#include "track.h"
#include "config.h"
#include "osc.h"

struct dsg_channel
{
    uint64_t time_total; // total amount of samples rendered

    enum dsg_chan_type  type     ;
    enum dsg_note_value base_note; // in semitones, 0x31 = 440 Hz

    // --- render state ---

    struct dsg_osc oscillator ;
    float          extra_pitch; // in semitones

    // --- track state ---

    enum dsg_effect_type cur_effect;

    float    duty_cycle ;
    float    duty_sweep ;
    float    pitch_slide;
    float    volume     ;
    float    volume_temp;
    float    volume_del ;
    float    harmony    ;
    float    h_amplitude;
    uint16_t arpeggio   ;
    int8_t   arp_speed  ;

    bool muted  ;
    bool playing;
};

void dsg_channel_init(struct dsg_channel* chan, struct dsg_cfg_chan* ccfg);

void dsg_channel_play(struct dsg_channel* chan);

void dsg_channel_upd (struct dsg_channel* chan, struct dsg_track_entry cur_entry);

void dsg_channel_free(struct dsg_channel* chan);

#endif

