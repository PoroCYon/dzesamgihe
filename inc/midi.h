
#ifndef DSG_MIDI_H
#define DSG_MIDI_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

enum dsg_midi_message
{
    dsg_midi_note_off = 0x80,
    dsg_midi_note_on  = 0x90,
    dsg_midi_pkeypres = 0xA0,
    dsg_midi_cchange  = 0xB0,
    dsg_midi_pchange  = 0xC0,
    dsg_midi_chanpres = 0xD0,
    dsg_midi_pitchbnd = 0xE0
};
enum dsg_midi_cc_chanmode
{
    dsg_midi_resetall = 0x79,
    dsg_midi_localctl = 0x7A,
    dsg_midi_allnoff  = 0x7B,
    dsg_midi_omnimoff = 0x7C,
    dsg_midi_omnimon  = 0x7D,
    dsg_midi_monomode = 0x7E,
    dsg_midi_polymode = 0x7F
};

enum dsg_midi_system
{
    // common
    dsg_midi_timingcd  = 0xF1,
    dsg_midi_songpos   = 0xF2,
    dsg_midi_songsel   = 0xF3,
    dsg_midi_tunereq   = 0xF4,
    // real-time
    dsg_midi_timingclk = 0xF8,
    dsg_midi_startseq  = 0xFA,
    dsg_midi_contseq   = 0xFB,
    dsg_midi_stopseq   = 0xFC,
    dsg_midi_activesns = 0xFE,
    dsg_midi_sysreset  = 0xFF,
    // sysex
    dsg_midi_sysex_start = 0xF0,
    dsg_midi_sysex_end   = 0xF7
};

#pragma pack(push, 1)
struct dsg_midi_raw
{
    uint8_t status;
    uint8_t data[2];
};
#pragma pack(pop)

/*struct dsg_midi_chanmsg
{
    size_t channel;
    enum dsg_midi_message msg_type;

    union
    {
        size_t note;
        enum dsg_midi_cc_chanmode cc_chanmode;
        size_t program;
        size_t pressure;
        size_t pb_msb;
    };

    union
    {
        size_t velocity;
        size_t cc_value;
        size_t pb_lsb;
    };
};
struct dsg_midi_sysmsg
{
    enum dsg_midi_system msg_type;

    union
    {
        uint8_t data[2];

        struct
        {
            size_t   sysex_len ;
            uint8_t* sysex_data;
        };
    };
};

enum dsg_midi_type
{
    dsg_midi_channel_message,
    dsg_midi_system_message ,
    dsg_midi_unknown_message
};

struct dsg_midi
{
    enum dsg_midi_type type;

    union
    {
        struct dsg_midi_raw     raw ;
        struct dsg_midi_chanmsg chan;
        struct dsg_midi_sysmsg  sys ;
    };
};

struct dsg_midi dsg_midi_parse(const struct dsg_midi_raw* raw);*/

size_t dsg_midi_packet_size(const uint8_t* raw);

struct dsg_midi_port
{
    uint32_t portnum;

    bool ignore_sysex;
    bool ignore_time ;
    bool ignore_sense;
};

#endif

