
#ifndef DSG_RENDER_H_
#define DSG_RENDER_H_

#include <stddef.h>
#include <stdint.h>
#include <math.h>

#include "track.h"
#include "chan.h"
#include "config.h"
#include "player.h"

#define ONE_TWELVETH (1.0f / 12.0f)

typedef void (*dsg_handle_buffer_fn)(size_t buf_len, void* buffer, void* userdata);
struct dsg_handle_buffer_cb
{
    dsg_handle_buffer_fn callback;
    void* userdata;
};

void dsg_render_chan  (struct dsg_player* player, size_t c, struct dsg_handle_buffer_cb  buf_handler );
void dsg_render_player(struct dsg_player* player          , struct dsg_handle_buffer_cb* buf_handlers);

inline static float dsg_note_to_freq(float note)
{
    // f = 2^((n - n_ref)/12) * f_ref
    return powf(2.0f, (note - dsg_note_A_4) * ONE_TWELVETH) * 440.0f;
}

#endif

