
#ifndef DSG_NET_LC_H_
#define DSG_NET_LC_H_

#include <stddef.h>
#include <stdint.h>

int dsg_net_lc_main(struct dsg_chan_hard_floppy_opts floppy_opts, /*const char* addr,*/ uint16_t port);

#endif

