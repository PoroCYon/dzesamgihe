
#ifndef DSG_DISABLE_FLOPPY
#ifndef DSG_FLOPPY_H_
#define DSG_FLOPPY_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "bufqueue.h"
#include "config.h"
#include "gpio.h"

struct dsg_floppy_ctl
{
    // if NULL, manual freq setting
    // will be used
    volatile struct dsg_buf_queue* queue;
    size_t pin_dir_bcm ;
    size_t pin_step_bcm;
    size_t cur_step;
    float cur_freq;
    bool quit_thread;
    bool cur_dir;
};

volatile struct dsg_floppy_ctl* dsg_floppy_make(
    struct dsg_chan_hard_floppy_opts opts);
void dsg_floppy_free(volatile struct dsg_floppy_ctl* ctl);

typedef void* dsg_floppy_bg_thread;

void   dsg_floppy_enqueue(dsg_floppy_bg_thread thread,
    size_t buf_size, float* buf, bool no_copy);
struct dsg_buf dsg_floppy_dequeue(dsg_floppy_bg_thread thread);

void dsg_floppy_set_freq(dsg_floppy_bg_thread thread, float freq); // 0 -> off

dsg_floppy_bg_thread dsg_floppy_spawn_bg_thread(volatile struct dsg_floppy_ctl* ctl, dsg_rpi_gpio gpio);
void  dsg_floppy_kill_bg_thread(dsg_floppy_bg_thread thread);

#endif
#endif

