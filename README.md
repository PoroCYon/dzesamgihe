# dzesamgi'e

A music tracker-like program for live floppy drive music on the Raspberry Pi. Does not expose a UI, you have to edit the `track.bin` file with your favourite hex editor.

## Dependencies

* `libc`
* `libm`
* `libpthread`
* `portaudio`
* `rtmidi`

